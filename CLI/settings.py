"""
Settings Class for settings of Application
"""
from __future__ import annotations

import json
import os
from enum import Enum
from functools import partial
from typing import get_type_hints

from jsonschema import validate, exceptions

from CLI import PrintSpeed
from game.data import Difficulty
from utils import Result


class Settings:
    """
    Class for handling user set settings management
    """
    __slots__ = ("print_speed", "difficulty", "player_name", "path")
    print_speed: PrintSpeed
    difficulty: Difficulty
    player_name: str
    path: str

    def __init__(
        self,
        print_speed: PrintSpeed = PrintSpeed.FAST,
        difficulty: Difficulty = Difficulty.NORMAL,
        player_name: str = "Player",
        path: str = None
    ):
        self.print_speed = print_speed
        self.difficulty = difficulty
        self.player_name = player_name
        if path is None:
            self.path = os.sep.join(__file__.split(os.sep)[:-1])
        else:
            self.path = path

    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "type": "object",
            "properties": {
                "print_speed": {"type": "integer"},
                "difficulty": {"type": "integer"},
                "player_name": {"type": "string"},
            },
            "required": [
                "print_speed",
                "difficulty",
                "player_name"
            ]
        }

    @classmethod
    def __new_save(cls, path: str) -> Settings:
        """
        Saves a new settings.json
        :return: the new Settings object
        """
        c = cls(path=path)
        c.save_to_file(path)
        return c

    @classmethod
    def load_from_file(cls, folder: str = None) -> Settings:
        """
        loads the settings from the save file
        :param folder: str - directory to load from
        :return: Settings
        """
        if not folder:
            folder = os.sep.join(__file__.split(os.sep)[:-1])

        try:
            with open(f'{folder}{os.sep}settings.json') as f:
                # no validation needed, because unaccepted input is caught in loads() and __init__()
                content = json.loads(f.read())
        except (IOError, json.decoder.JSONDecodeError):
            return cls.__new_save(f'{folder}')
        try:
            validate(content, cls._schema())
        except exceptions.ValidationError:
            return cls.__new_save(f'{folder}')
        return cls(**{name: type_(content[name]) for name, type_ in get_type_hints(cls).items()})

    def save_to_file(self, path: str = None) -> None:
        """
        saves the settings to the save file
        :param path: str - target directory
        :return: None
        """
        if not path:
            path = self.path

        get = partial(getattr, self)
        dump = {name: get(name).value if isinstance(get(name), Enum) else get(name) for name in self.__slots__}
        dump = json.dumps(dump)
        with open(f'{path}{os.sep}settings.json', "w") as f:
            f.write(dump)

    def __give_help(self) -> str:
        """
        :return: help string for this class
        """
        return "".join(f"\t{name}: {getattr(self, name)}\n" for name in self.__slots__)

    def __help_parameter(self, param: str) -> str:
        """
        :param param: the param which help is needed for
        :return: the current value of the parameter (text)
        """
        try:
            return f"\t{param}: {getattr(self, param)}\n" \
                   f"\tIf you want to change the Parameter, use 'settings <parameter> <new_value>'"
        except AttributeError:
            return f"\tAttribute '{param}' doesn't exists"

    def __set_attr(self, name: str, value: str) -> str:
        # cls(**{name: type_(content[name]) for name, type_ in get_type_hints(cls).items()})
        difficulties = tuple(v.name.lower() for v in Difficulty)
        try:
            getattr(self, name)
            if value.isdigit():
                value = int(value)
            elif value.lower() in difficulties:
                value = difficulties.index(value.lower()) + 1
            value = get_type_hints(self.__class__)[name](value)
            setattr(self, name, value)
            return f"\t{name} set to {value} successfully"
        except AttributeError:
            return f"\tAttribute '{name}' doesn't exists"
        except ValueError:
            return f"\t{value} is not a valid value for {name}"

    # noinspection PyIncorrectDocstring,PyUnusedLocal
    def set(self, *args) -> Result:
        """
        Sets the param_name to the value of param.
        If no args are passed on, it instead returns the current settings
        :param param_name: str - the name of the attribute you want to edit
        :param value: str - the value you want it to set to
        :return: None
        """
        if not args:
            ans = self.__give_help()
        elif len(args) == 1:
            ans = self.__help_parameter(args[0])
        elif len(args) == 2:
            ans = self.__set_attr(args[0], args[1])
        else:
            ans = f"\tTo many arguments: {args}"

        self.save_to_file()
        return Result(ans, self)
