"""
functions handling the players interaction with NPCs
"""
from functools import partial

from CLI import GameState, show, PrintSpeed, show_dialog, parsing_loop
from game.data import DialogBranch
from game.logic import list_branches, \
    input_to_dialog_branch_mapping, handle_quest
from utils import Result


def inquire_user(prompt: str, accepted: {str: any}, fail_response: str) -> Result:
    """
    gets a predefined answer from the user.
    Only use this for quick confirmations like "yes/no" questions
    :param prompt: text to display
    :param accepted: dictionary of accepted inputs and their return values
    :param fail_response: if the user types a wrong string
    :return: Result(msg = user_input, value = accepted[user_input])
    """
    while True:
        user_input = input(prompt).lower().strip()
        if user_input in accepted:
            return Result(user_input, accepted[user_input])
        else:
            show(fail_response)


def talk_and_recurse(
    branch: DialogBranch,
    npc_name: str,
    game_state: GameState,
    base_register: {str, callable}
) -> GameState:
    """
    shows the dialog response and descends the dialog tree
    :param branch: the root branch
    :param npc_name: the name of the npc
    :param game_state: the current game state
    :param base_register: additionally permitted commands
    :return: the game state after the dialog
    """
    show_dialog(branch.response)

    if branch.quest:
        res = handle_quest(branch.quest, game_state.player, show_dialog, inquire_user)
        game_state.player = res.value
        show(res.message)

    if not branch.branches:
        _, game_state = parsing_loop(
            game_state.current_location.register,
            game_state.current_location.name,
            game_state
        )
    else:
        show(
            list_branches(branch.branches, game_state.player),
            PrintSpeed.INSTANT
        )
        accepted_inputs = {} | base_register
        mapping = input_to_dialog_branch_mapping(branch.branches, game_state.player)
        for key, branch_ in mapping.items():
            accepted_inputs[key] = partial(talk_and_recurse, branch_, npc_name, base_register=base_register)

        _, game_state = parsing_loop(accepted_inputs, npc_name, game_state)
    return game_state
