"""
constructors for different guys
"""
import os

from CLI import GameState, Settings, action_registers, char_creation_dialog, save_games_state
from game.data import Location, parse_locations, MetaData, load_player, Player
from utils.errors import CorruptFile, PawnDoesNotExist


def load_locations(path: str) -> dict[str, Location]:
    """
    loads all locations from the given directory
    :param path: locations directory
    :return: {name: location}
    """
    parsed_locations = parse_locations(path)

    ans = {}
    for name, location in parsed_locations.items():
        if not isinstance(location.register, dict):
            location.register = getattr(action_registers, location.register)()
        ans[name] = location

    return ans


def load_initial_game_state(settings: Settings, meta: MetaData) -> GameState:
    """
    :return: a valid game state dict
    """

    try:  # <- I don't like "try" here, but I like the flexibility that "all attribute setting" provides more
        player: Player = load_player(settings.player_name)
    except IOError:
        print(f"{settings.player_name} is not an existing character")
        raise PawnDoesNotExist(f"{settings.player_name} is not an existing character")
    except CorruptFile:
        print(f"Save data of {settings.player_name} is corrupted")
        raise PawnDoesNotExist(f"{settings.player_name} is corrupted")

    location_name = player.location_name

    locations = load_locations(f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}locations")

    # noinspection PyTypeChecker
    return GameState(
        player,
        None,
        locations,
        locations[location_name],
        settings,
        meta
    )


def create_new_character(settings: Settings, meta: MetaData) -> GameState:
    """
    character creation dialog handler
    :param settings:
    :param meta:
    :return: game ready game state
    """
    player_name = char_creation_dialog()

    settings.player_name = "default"
    gs = load_initial_game_state(settings, meta)

    gs.settings.player_name = player_name
    gs.player.pawn.name = player_name
    save_games_state(gs)
    gs.settings.save_to_file()

    return gs


if __name__ == "__main__":
    ...
