"""
action procedures for execution
This is also the lowest layer before IO. -> Prints are allowed in here!
"""
import io
from copy import deepcopy

from CLI import parsing_loop, GameState, show, save_games_state, Settings, PrintSpeed, check_stack_size, \
    talk_and_recurse, show_dialog
from game.data import StatNames, Pawn, load_player, NPC
from game.logic import basic_attack, enemy_turn, end_turn, stats_up, str_unlocked_skills, str_unlockable_skills, \
    knowable_skills_map, skill_up, max_mana, max_hit_points, defend, use_skill, grant_item, all_skills, scale_monster, \
    repr_fight_status, abbreviation_to_key_word, repr_player_detailed, repr_player, level_monster, greet_player, \
    inventory_to_str, str_upgradeable_stats
from utils import Result, rng, box_in
from utils.errors import SkillOnCooldown, NotEnoughMana


def __enemy_action(game_state: GameState, player: Pawn, enemy: Pawn):
    result = enemy_turn(player, enemy)
    show(result.message, game_state.settings.print_speed)
    enemy, player = result.value  # reverse player attack
    result = end_turn(player, enemy, game_state.settings.difficulty)
    show(result.message, game_state.settings.print_speed)
    game_state.player.pawn, game_state.enemy = result.value
    return game_state.enemy.stats.hit_points > 0, game_state


def basic_attack_action(game_state: GameState) -> (bool, dict):
    """
    fights an enemy
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    result = basic_attack(game_state.player.pawn, game_state.enemy)
    show(result.message)
    continuation, game_state = __enemy_action(game_state, *result.value)
    if continuation:
        show(repr_fight_status(game_state.player.pawn, game_state.enemy), PrintSpeed.INSTANT)
    return continuation, game_state


def defend_action(game_state: GameState) -> (bool, dict):
    """
    defends for one turn
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    result = defend(game_state.player.pawn)
    show(result.message)
    game_state.player.pawn = result.value
    continuation, game_state = __enemy_action(game_state, game_state.player.pawn, game_state.enemy)
    show(repr_fight_status(game_state.player.pawn, game_state.enemy), PrintSpeed.INSTANT)
    return continuation, game_state


def use_skill_action(*name: str, game_state: GameState) -> tuple[bool, GameState]:
    """
    tries to use a skill
    :param name: name of the skill
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    name = "_".join(name).lower()
    if name not in all_skills():
        show(f"\tSkill '{name}' does not exist")
        return True, game_state
    if name not in game_state.player.pawn.skills:
        show(f"\tYou have not learned '{name}' yet")
        return True, game_state

    try:
        result = use_skill(game_state.player.pawn, name, game_state.enemy)
        show(result.message)
    except SkillOnCooldown as e:
        show(e.args[0])
        return True, game_state
    except NotEnoughMana as e:
        show(e.args[0])
        return True, game_state

    continuation, game_state = __enemy_action(game_state, *result.value)
    if continuation:
        show(repr_fight_status(game_state.player.pawn, game_state.enemy), PrintSpeed.INSTANT)
    return continuation, game_state


def change_location_action(*key_word: str, game_state: GameState) -> tuple[bool, GameState]:
    """
    moves the character from place to place
    :param key_word: user invoked location token
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    key_word = "_".join(key_word).lower()  # name entered with spaces support
    alt_key_word = f"{game_state.current_location.name}_{key_word}".lower()

    if key_word not in game_state.locations:
        if alt_key_word not in game_state.locations:
            show(f"\tLocation '{key_word}' does not exist")
            return True, game_state
        else:
            key_word = alt_key_word

    msg = check_stack_size(game_state.meta)
    if msg:
        show(msg, game_state.settings.print_speed)

    location = game_state.locations[key_word]
    # TODO: location.name in game_state.player.known_locations and ...
    if key_word in game_state.current_location.connected:
        # start new parsing loop with location register
        game_state.current_location = location
        _, game_state = parsing_loop(location.register, location.name, game_state)
    else:
        show(location.locked_phrase)
    return True, game_state


def list_locations_action(game_state: GameState) -> tuple[bool, GameState]:
    """
    lists the currently available & connected locations
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    ans = io.StringIO()
    for name, loc in game_state.locations.items():
        # TODO: location.name in game_state.player.known_locations and ...
        if name in game_state.current_location.connected:
            ans.write(f"\t{loc.name}\n")
    show(ans.getvalue(), game_state.settings.print_speed)
    return True, game_state


def search_enemy_action(fight_register: {str: callable}, game_state: GameState) -> tuple[bool, GameState]:
    """
    search in an area to spawn treasures, events or enemies
    :param fight_register:
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    choice = rng(game_state.meta.action_counter)
    spawns = game_state.current_location.spawns
    spawned = deepcopy(spawns[choice % len(spawns)])
    if spawned is None:
        show("\tYou searched found nothing...")
    elif isinstance(spawned, Pawn):
        show(f"\tFound monster {spawned.name}!")
        game_state.enemy = level_monster(spawned, game_state.current_location, game_state.meta.action_counter)
        game_state.enemy = scale_monster(spawned, game_state.settings.difficulty)
        show(repr_fight_status(game_state.player.pawn, game_state.enemy), PrintSpeed.INSTANT)
        _, game_state = parsing_loop(fight_register, "fight", game_state)
    else:  # isinstance(spawned, Item)
        result = grant_item(spawned, game_state.player.pawn)
        show(result.message)
        game_state.player.pawn = result.value

    game_state.meta.action_counter += 1
    return True, game_state


def invest_skill_points(*key_words: (str,), game_state: GameState) -> tuple[bool, GameState]:
    """
    invest the skill points of the player character in stats
    :param key_words: user invoked stat name
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    if game_state.player.pawn.sp < 1:
        show("\tYou don't have any Skill Points (SP) left to invest")
        return True, game_state

    input_ = " ".join(key_words).lower()
    key_word = abbreviation_to_key_word(input_, game_state.player.pawn.skills)
    knowable = (key for real_name, key in knowable_skills_map(game_state.player.pawn.skills).items())

    if key_word in knowable:
        result = skill_up(game_state.player.pawn, key_word)
    elif key_word in {elem for elem in StatNames}:
        result = stats_up(game_state.player.pawn, StatNames(key_word))
    else:
        result = Result(f"Name {key_word} is not defined. Did you make a typo?", None)

    show(result.message)
    return True, game_state


def show_unlocked_skills_action(game_state: GameState) -> tuple[bool, GameState]:
    """
    displays the learnable skills to the viewer
    :param game_state:
    :return:
    """
    game_state.meta.action_counter += 1

    show(str_unlocked_skills(game_state.player.pawn.skills), game_state.settings.print_speed)
    return True, game_state


def show_upgradeable_action(game_state: GameState) -> tuple[bool, GameState]:
    """
    displays the learned skills to the viewer
    :param game_state:
    :return:
    """
    game_state.meta.action_counter += 1

    show(
        "available skills:\n  " + str_unlockable_skills(game_state.player.pawn.skills), game_state.settings.print_speed
    )
    show(str_upgradeable_stats(game_state.player.pawn.stats), game_state.settings.print_speed)
    if game_state.player.pawn.skills:
        show("upgradeable skills:\n  " + str_unlocked_skills(game_state.player.pawn.skills))
    return True, game_state


def show_status(*detailed: (str,), game_state: GameState) -> tuple[bool, GameState]:
    """
    shows the player his own current status.
    :param detailed: the player can add the "detailed" flag for a more detailed output
    :param game_state: the current game state
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    if detailed:
        if detailed[0] == "detailed" or detailed[0] == "full":
            show(repr_player_detailed(game_state.player.pawn), PrintSpeed.INSTANT)
        else:
            show(f"\tThe given flag {detailed[0]} is unknown.\n"
                 f"\tPlease type \"status\" or \"status detailed\"")
    else:
        show(repr_player(game_state.player.pawn), PrintSpeed.INSTANT)
    return True, game_state


def rest_in_inn(game_state: GameState) -> (bool, dict):
    """
    rest at the inn and recover lost hp and mana
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    player = game_state.player
    player.pawn.stats.mana = max_mana(player.pawn)
    player.pawn.stats.hit_points = max_hit_points(player.pawn)
    player.pawn.buffs = []  # removes buffs
    player.pawn.status_effects = []  # and status effects
    game_state.player = player
    save_games_state(game_state)

    show("\tThe game was saved successfully")

    return True, game_state


def set_settings(*args, game_state: GameState) -> tuple[bool, GameState]:
    """
    without arguments -> shows the current settings
    with <key> -> shows the value of the <key>
    with <key> <value> -> sets the <key> to the new <value>
    :return: loop continuation, new games state
    """
    settings: Settings = game_state.settings
    result = settings.set(*args)
    game_state.settings = result.value
    show(result.message, settings.print_speed)

    return True, game_state


def load_character(*args, game_state: GameState) -> tuple[bool, GameState]:
    """
    loads the character with given name and sets him as current character
    :param args: character name
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    # TODO: implement character switching
    name, _ = args

    player = load_player(name)
    game_state.player = player.pawn
    game_state.current_location = game_state.locations[player.location_name]

    return True, game_state


def talk_to_npc(*args, game_state: GameState, base_register: dict[str, callable]) -> tuple[bool, GameState]:
    """
    talks to the npc with the provided name.
    :param args: npc name
    :param game_state:
    :param base_register:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    if not args:
        return True, game_state
    npc_name: str = args[0]
    try:
        npc: NPC = game_state.current_location.npc_s[npc_name.lower()]
    except KeyError:
        show(f"\tThe NPC with name '{npc_name}' does not exist.")
        return True, game_state

    text = greet_player(npc, game_state.meta.action_counter)
    show_dialog(text)

    game_state = talk_and_recurse(npc.root_branch, npc.name, game_state, base_register)

    return True, game_state


# noinspection PyUnusedLocal
def list_npc_s(*args, game_state: GameState) -> tuple[bool, GameState]:
    """
    lists all NPCs in the area
    :param args: are getting ignored
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1

    show("".join(f"\t{name} ({title})\n" if title else f"\t{name}\n" for name, title in
                 tuple((npc.name, npc.title) for _, npc in game_state.current_location.npc_s.items()))
         )
    return True, game_state


def show_inventory(*args, game_state: GameState) -> tuple[bool, GameState]:
    """
    shows the current inventory of the player to the player
    :param args: are getting ignored
    :param game_state:
    :return: loop-continuation (bool), game_state
    """
    game_state.meta.action_counter += 1
    inventory_list = inventory_to_str(game_state.player.pawn.inventory, args)
    ans = box_in(inventory_list, box_char='*') if inventory_list else f"You don't have an item called {args[0]}"
    show(ans, PrintSpeed.INSTANT)
    return True, game_state
