"""
Settings relating Enums
"""
import enum


class PrintSpeed(enum.Enum):
    """
    Speeds at which to present
    """
    SUPER_SLOW = 50
    SLOW = 20
    NORMAL = 10
    FAST = 5
    FASTER = 2
    VERY_FAST = 1
    INSTANT = 0


if __name__ == "__main__":
    ...
