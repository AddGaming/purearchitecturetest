"""
Only exists to solve import conflict.
"""
from dataclasses import dataclass

from game.data import Pawn, Location, player_to_file, MetaData, meta_to_file, Player
from CLI import Settings


@dataclass(slots=True)
class GameState:
    """
    Keeps track of the current game state
    """
    player: Player
    enemy: Pawn  # Default dummy if currently not fighting
    locations: dict[str, Location]
    current_location: Location
    settings: Settings
    meta: MetaData


def save_games_state(gs: GameState) -> None:
    """
    saves the game to disc
    :param gs: current game state
    """
    loc_stem = gs.current_location.name.lower()
    location_code = loc_stem
    i = 0
    while location_code not in gs.locations:  # has to terminate since the player can only be at a valid location
        location_code = f"{gs.current_location.connected[i]}_{location_code}"
        i += 1

    player_to_file(gs.player, location_code)
    meta_to_file(gs.meta)


if __name__ == "__main__":
    ...
