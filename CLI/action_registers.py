"""
allowed actions and "state" management
"""
from functools import partial
from CLI.actions import basic_attack_action, invest_skill_points, change_location_action, rest_in_inn, \
    show_unlocked_skills_action, show_upgradeable_action, set_settings, search_enemy_action, use_skill_action, \
    defend_action, list_locations_action, show_status, list_npc_s, talk_to_npc, show_inventory
from CLI import give_help, parsing_loop, quit_, break_out
from game.data import load_encyclopedia


def _menu_able_register() -> {str: callable}:
    fun = partial(parsing_loop, registered_menu(), "menu")
    fun.__doc__ = parsing_loop.__doc__
    return {
        "menu": fun,
    }


def _basic_register() -> {str: callable}:
    return {
        "settings": set_settings,
        "inventory": show_inventory,
        "?inventory": show_inventory,
        "status": show_status,
        "?status": show_status,
        "quit": quit_,
        "exit": quit_,
    }


def _moving_register() -> {str: callable}:
    return {
        "move": change_location_action,
        "visit": change_location_action,
        "?connected": list_locations_action,
        "?visit": list_locations_action,
        "?move": list_locations_action,
    }


def registered_menu() -> {str: callable}:
    """
    :return: new register with cli token mapping
    """
    skill_fun = partial(parsing_loop, registered_skill_up(), "skill'ing")
    skill_fun.__doc__ = registered_skill_up.__doc__
    register = {
        "skill": skill_fun,
        "skills": skill_fun,
        "skilling": skill_fun,
        "skill_up": skill_fun,
        "back": break_out,
    }
    register |= _basic_register()
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register


def register_monster_zone() -> {str: callable}:
    """
    :return: new register with cli token mapping
    """
    search_fun = partial(search_enemy_action, registered_combat())
    search_fun.__doc__ = search_enemy_action.__doc__
    register = {
        "search": search_fun,
    }
    register |= _basic_register()
    register |= _moving_register()
    register |= _menu_able_register()
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register


def registered_combat() -> {str: callable}:
    """
    :return: new register with cli token mapping
    """
    register = {
        "attack": basic_attack_action,
        "atk": basic_attack_action,
        "skill": use_skill_action,
        "use": use_skill_action,
        "?skill": show_unlocked_skills_action,
        "?skills": show_unlocked_skills_action,
        "defend": defend_action,
        "run": break_out,
    }
    register |= _basic_register()
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register


def registered_skill_up() -> {str: callable}:
    """
    a menu for leveling up different aspects of the character
    :return: new register with cli token mapping
    """
    register = {
        "inc": invest_skill_points,
        "skill": invest_skill_points,
        "increase": invest_skill_points,
        "level": invest_skill_points,
        "learn": invest_skill_points,

        "?learnable": show_upgradeable_action,
        "learnable": show_upgradeable_action,
        "?inc": show_upgradeable_action,
        "?increase": show_upgradeable_action,
        "?level": show_upgradeable_action,
        "?learn": show_upgradeable_action,

        "?skills": show_unlocked_skills_action,
        "?skill": show_unlocked_skills_action,
        "?learned": show_unlocked_skills_action,

        "back": break_out,
        "menu": break_out,
    }
    register |= _basic_register()
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register


def register_inn() -> {str: callable}:
    """
    :return: new register with cli token mapping for "inn" location
    """
    register = {
        "rest": rest_in_inn,
        "save": rest_in_inn,
    }
    register |= _basic_register()
    register |= _moving_register()
    register |= _menu_able_register()
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register


def register_dialog() -> {str: callable}:
    """
    :return: new register with cli token mapping for town locations
    """
    register = {
        "back": break_out
    }
    register |= _basic_register()
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register


def register_town() -> {str: callable}:
    """
    :return: new register with cli token mapping for town locations
    """
    register = _basic_register()
    register |= _moving_register()
    register |= _menu_able_register()
    register["?talk"] = list_npc_s
    talk_fun = partial(talk_to_npc, base_register=register_dialog())
    talk_fun.__doc__ = talk_to_npc.__doc__
    register["talk"] = talk_fun
    register["help"] = partial(give_help, register.copy(), load_encyclopedia())
    return register
