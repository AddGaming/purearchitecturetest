"""
functionality for the CLI
"""
import string
import sys
from time import sleep

from CLI import Settings, PrintSpeed, GameState
from game.data import MetaData, Dialog
from utils import box_in, color, tab_right
from utils import Colors


# noinspection PyUnusedLocal
def give_help(register: {str: callable}, encyclopedia: {str: str}, *args, **kwargs) -> list[bool]:
    """
    prints the documentation of all registered functions to the screen.
    :return: loop continuation
    """
    if args:
        args = tuple(s.lower() for s in args)
        if args[0] == "q":
            show(" | ".join(key for key in register), PrintSpeed.INSTANT)
        elif args[0] in encyclopedia:
            show(f"\n{encyclopedia[args[0]]}\n", PrintSpeed.INSTANT)
        elif args[0] in register:
            show(f"\t{args[0]}:\n{register[args[0]].__doc__.split(':')[0]}", PrintSpeed.INSTANT)
        else:
            show(f"The keyword '{args[0]}' does not exist")
    else:
        output_msgs = {}
        for key, val in register.items():
            msg = val.__doc__.split(':')[0]
            if msg not in output_msgs:
                output_msgs[msg] = key
            else:
                output_msgs[msg] += f" | {key}"

        for msg, key in output_msgs.items():
            show(f"{key}: {msg}", PrintSpeed.INSTANT)
    return [True]


def parsing_loop(
    registered: {str: callable},
    loop_name: str,
    game_state: GameState,
    default_message: str = ""
) -> (bool, dict):
    """
    Parsing user input and executing mapper
    :param game_state: current game state
    :param loop_name:
    :param registered:
    :param default_message:
    :return: loop continuation, new games state
    """
    game_state.meta.recursion_counter += 1

    # no crashes with this, but constant player nagging
    if game_state.meta.recursion_counter > sys.getrecursionlimit() - 5:
        sys.setrecursionlimit(sys.getrecursionlimit() + 100)

    running = True
    while running:
        cmd_param = input(f"{loop_name} $: ").lower().strip()
        if not cmd_param:
            continue

        if cmd_param.split()[0] in registered:
            running, *new_state = registered[cmd_param.split()[0]](
                *cmd_param.split()[1:],
                **{"game_state": game_state}
            )
            if new_state:
                game_state = new_state[0]
        elif cmd_param in registered:
            running, *new_state = registered[cmd_param](
                **{"game_state": game_state}
            )
            if new_state:
                game_state = new_state[0]
        else:
            if default_message:
                show(default_message)
            else:
                show(f"unrecognized input: {cmd_param.split()[0]}", PrintSpeed.INSTANT)

    game_state.meta.recursion_counter -= 1
    return True, game_state


# print_slow, print_normal -> show
def show(output: str, speed: PrintSpeed = None, c: Colors = None) -> None:
    """
    displays strings to the user
    :param speed: the speed of display
    :param output: string to show
    :param c: color of the output
    :return: None
    """
    if not output:
        output = "\t"

    if not speed:
        speed = Settings.load_from_file().print_speed

    p_speed = speed.value / 100

    if c:
        output = color(output, c)

    for letter in output[:-1]:  # chopping last character...
        print(letter, end="", flush=True)
        if letter not in string.whitespace:
            sleep(p_speed)
    print(output[-1], end=f"{Colors.ENDC}\n")  # ...to print him with \n


def show_dialog(dialog: Dialog) -> None:
    """
    a special show function for dialogs which are processed recursively
    :param dialog: the dialog to show
    :return:
    """
    if not dialog.text:
        return
    show(tab_right(dialog.text), PrintSpeed(dialog.speed))
    if dialog.follow_up:
        show_dialog(dialog.follow_up)


# noinspection PyUnusedLocal
def break_out(*args, **kwargs) -> list[bool]:
    """
    return to the previous menu
    :return: loop discontinuation
    """
    return [False]


# noinspection PyUnusedLocal
def quit_(*args, **kwargs) -> None:
    """
    quits the programm
    :return:
    """
    quit()


def check_stack_size(meta: MetaData) -> str:
    """
    checks the current stack size and warns the user if he is about to exceed the default limit
    :return: user message
    """
    rec_limit = sys.getrecursionlimit()
    if meta.recursion_counter > rec_limit - 100:
        return f"\tYou should consider taking a break.\n" \
               f"\tIf you continue to play, it could have a negative impact on the performance on your machine!"
    elif meta.recursion_counter == rec_limit - 200 or meta.recursion_counter == rec_limit - 150:
        return f"\tYou are playing for a rather long time already\n" \
               f"\tConsider taking a break soon."
    else:
        return ""


# noinspection PyUnboundLocalVariable
def char_creation_dialog() -> str:
    """
    the dialog for creating a new player character
    :return: the parameters for a new player (atm only name:str)
    """
    proceed = False
    while not proceed:
        name = input("Please enter you characters name: ").strip()
        if name.lower() == "quit" or name.lower() == "exit":
            quit_()

        confirm = input(f"So your character is named \"{name}\"?\n"
                        f"\t(y/n):")
        if confirm.lower() == "y" or confirm.lower() == "yes":
            proceed = True

    show(box_in(
        f"You will explore a wide interconnected world of monster, magic and wonder.\n"
        f"The CLI has the following format:\n\n"
        f"Inn $: ?move\n"
        f" ^   ^   ^  \n"
        f" |   |   your command\n"
        f" |  seperator\n"
        f"Your current location\n\n"
        f"You can allways list all your currently available commands with the 'help' command\n"
        f"You can also use the 'help' command like this to receive help on a specific topic:\n\n"
        f"Inn $: help hit-points\n"
        f"         ^      ^  \n"
        f"         |    the key word, you want to know more about\n"
        f"        your command ('help')\n\n"
        f"Your games saves only if you are 'rest'-ing at an inn.\n"
        f"Have nice travels"
    ), PrintSpeed.VERY_FAST)

    return name
