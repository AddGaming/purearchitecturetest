"""
Monster scaling
"""
import unittest

from game.data import Difficulty, Pawn
from game.logic import scale_monster


class SinglePlayerTests(unittest.TestCase):

    def test_lowest_difficulty_changes_nothing(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.EASY
        monster = scale_monster(monster, diff)
        self.assertEqual(monster.stats.hit_points, 10, f"{monster.stats.hit_points=} should equal 10")
        self.assertEqual(monster.stats.attack, 10, f"{monster.stats.attack=} should equal 10")
        self.assertEqual(monster.stats.defense, 10, f"{monster.stats.defense=} should equal 10")

    def test_higher_difficulty_changes_hp(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.NORMAL
        monster = scale_monster(monster, diff)
        self.assertTrue(monster.stats.hit_points > 10, f"{monster.stats.hit_points=} should > 10")

    def test_higher_difficulty_changes_atk(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.NORMAL
        monster = scale_monster(monster, diff)
        self.assertTrue(monster.stats.attack > 10, f"{monster.stats.attack=} should > 10")

    def test_higher_difficulty_changes_def(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.NORMAL
        monster = scale_monster(monster, diff)
        self.assertTrue(monster.stats.defense > 10, f"{monster.stats.defense=} should > 10")

    def test_higher_difficulty_does_not_change_mana(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.NORMAL
        monster = scale_monster(monster, diff)
        self.assertTrue(monster.stats.mana == 10, f"{monster.stats.mana=} should equal 10")


class MultiPlayerTests(unittest.TestCase):

    def test_more_players_changes_hp(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.EASY
        monster = scale_monster(monster, diff, 2)
        self.assertTrue(monster.stats.hit_points > 10, f"{monster.stats.hit_points=} should > 10")

    def test_more_players_changes_hp_additionally_to_difficulty(self):
        monster1 = Pawn.dummy(atk=10, defense=10)
        monster2 = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.NORMAL
        monster1 = scale_monster(monster1, diff)
        monster2 = scale_monster(monster2, diff, 2)
        self.assertTrue(
            monster2.stats.hit_points > monster1.stats.hit_points,
            f"{monster2.stats.hit_points=} should > {monster1.stats.hit_points=}"
        )

    def test_more_players_changes_mana(self):
        monster = Pawn.dummy(atk=10, defense=10)
        diff = Difficulty.EASY
        monster = scale_monster(monster, diff, 2)
        self.assertTrue(monster.stats.mana > 10, f"{monster.stats.mana=} should > 10")


if __name__ == '__main__':
    unittest.main()
