"""
Test that monster get leveled correctly
"""
import os
import unittest
from copy import deepcopy

from CLI.factories import load_locations
from game.data import load_pawn, Location
from game.logic import level_monster


class TestLevelMonster(unittest.TestCase):

    def test_monster_level_is_in_bounds(self):
        dummy = load_pawn(
            "dummy_pawn",
            f"{os.getcwd()}{os.sep}tests{os.sep}mocks"
        )
        test_location: Location = load_locations(
            f"{os.getcwd()}{os.sep}tests{os.sep}mocks{os.sep}test_location"
        )["test_area"]
        lower = test_location.level_ranges["dummy_pawn"][0]
        upper = test_location.level_ranges["dummy_pawn"][1]

        for i in range(1000):
            new_monster = level_monster(deepcopy(dummy), test_location, i)
            self.assertTrue(lower <= new_monster.lv <= upper)


if __name__ == '__main__':
    unittest.main()
