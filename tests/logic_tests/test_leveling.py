"""
Tests for leveling the player
"""
import unittest
from copy import deepcopy

from game.data import StatNames, Pawn
from game.logic import stats_up
from game.logic.leveling import level_thresh_hold, level_up


class TestLevelUp(unittest.TestCase):

    def test_character_levels_up(self):
        default_dummy = Pawn.dummy()
        default_dummy.lv = 1
        default_dummy.xp = level_thresh_hold(1)
        res = level_up(default_dummy)
        self.assertTrue(res.value.lv == 2)

    def test_character_levels_up_two_times(self):
        default_dummy = Pawn.dummy()
        default_dummy.lv = 1
        default_dummy.xp = level_thresh_hold(1) + level_thresh_hold(2)
        res = level_up(default_dummy)
        self.assertTrue(res.value.lv == 3)

    def test_level_up_increases_sp(self):
        default_dummy = Pawn.dummy()
        default_dummy.sp = 0
        default_dummy.lv = 1
        default_dummy.xp = level_thresh_hold(1)
        res = level_up(default_dummy)
        self.assertTrue(res.value.sp == 1)


class TestLevelThreshHold(unittest.TestCase):

    def test_level_thresh_hold_increases_with_level(self):
        t1 = level_thresh_hold(1)
        t2 = level_thresh_hold(2)
        self.assertTrue(t1 < t2)


class TestStatUp(unittest.TestCase):

    def test_all_stats_supported(self):
        default_dummy = Pawn.dummy()
        default_dummy.sp = 1000
        for stat_name in StatNames:
            res = stats_up(deepcopy(default_dummy), stat_name)
            self.assertNotEqual(hash(res.value), hash(default_dummy))

    def test_increasing_stat_decreases_sp(self):
        default_dummy = Pawn.dummy()
        default_dummy.sp = 1000
        res = stats_up(deepcopy(default_dummy), StatNames.HIT_POINTS)
        self.assertTrue(res.value.sp < default_dummy.sp)


if __name__ == '__main__':
    unittest.main()
