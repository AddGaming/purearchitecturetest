"""
Test universal properties of the stat calculation
without specific values since formulas can change
"""
import unittest

from game.data import Buff, Pawn, StatNames
from game.logic import max_hit_points, max_mana


class TestMaxMana(unittest.TestCase):

    def test_higher_level_means_higher_mana(self):
        dummy1 = Pawn.dummy(mana_level=2)
        dummy2 = Pawn.dummy(mana_level=3)
        self.assertTrue(max_mana(dummy1) < max_mana(dummy2))

    def test_higher_base_means_higher_mana(self):
        dummy1 = Pawn.dummy(mana=1)
        dummy2 = Pawn.dummy(mana=2)
        self.assertTrue(max_mana(dummy1) < max_mana(dummy2))

    def test_buffs_get_accounted_for(self):
        dummy1 = Pawn.dummy()
        dummy2 = Pawn.dummy()
        m_buff = Buff(StatNames.MANA, 1, 5, 5)
        dummy2.buffs.append(m_buff)
        self.assertTrue(max_mana(dummy1) < max_mana(dummy2))


class TestMaxHP(unittest.TestCase):

    def test_higher_level_means_more_hp(self):
        dummy1 = Pawn.dummy(hit_points_level=2)
        dummy2 = Pawn.dummy(hit_points_level=3)
        self.assertTrue(max_hit_points(dummy1) < max_hit_points(dummy2))

    def test_higher_base_means_higher_hp(self):
        dummy1 = Pawn.dummy(hp=1)
        dummy2 = Pawn.dummy(hp=2)
        self.assertTrue(max_hit_points(dummy1) < max_hit_points(dummy2))

    def test_buffs_get_accounted_for(self):
        dummy1 = Pawn.dummy()
        dummy2 = Pawn.dummy()
        m_buff = Buff(StatNames.HIT_POINTS, 1, 5, 5)
        dummy2.buffs.append(m_buff)
        self.assertTrue(max_hit_points(dummy1) < max_hit_points(dummy2))


if __name__ == '__main__':
    unittest.main()
