"""
Testing universal properties on locations
"""
import os
import unittest

from CLI.factories import load_locations


class TestProperties(unittest.TestCase):

    def test_all_locations_can_quit_the_game(self):
        locations = load_locations(
            f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}locations"
        )
        for name, location in locations.items():
            self.assertTrue("quit" in location.register)

    def test_all_locations_have_at_leased_one_connections(self):
        locations = load_locations(
            f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}locations"
        )
        for name, location in locations.items():
            self.assertTrue(len(location.connected) >= 1)


if __name__ == '__main__':
    unittest.main()
