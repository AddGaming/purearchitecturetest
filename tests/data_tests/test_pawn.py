"""
Tests Pawn basic properties
"""
import unittest

from game.data import Pawn


class HashProperties(unittest.TestCase):
    def test_same_pawn_same_hash(self):
        p1 = Pawn.dummy()
        self.assertEqual(hash(p1), hash(p1))

    # noinspection PyArgumentEqualDefault
    def test_slightly_different_constructed_pawn_different_hash(self):
        p1 = Pawn.dummy(10)
        h1 = hash(p1)
        p2 = Pawn.dummy(9)
        h2 = hash(p2)
        self.assertNotEqual(h1, h2)

    def test_different_pawn_different_hash(self):
        p1 = Pawn.dummy()
        p2 = Pawn.dummy(name="Dummy2")
        self.assertNotEqual(hash(p1), hash(p2))

    def test_same_pawn_different_constructors_same_hash(self):
        p1 = Pawn.dummy()
        p2 = Pawn.dummy()
        self.assertEqual(hash(p1), hash(p2))

    def test_hash_greater_one_million(self):
        for i in range(100):
            p = Pawn.dummy(i)
            h = hash(p)
            self.assertTrue(h > 1_000_000, f"{h=} < 1_000_000")

    def test_luck_has_impact(self):  # luck is a branch in the hash function
        p1 = Pawn.dummy(luck=0)
        p2 = Pawn.dummy(luck=3)
        self.assertNotEqual(hash(p1), hash(p2))

    def test_defense_has_impact(self):  # defense is a branch in the hash function
        p1 = Pawn.dummy(defense=0)
        p2 = Pawn.dummy(defense=3)
        self.assertNotEqual(hash(p1), hash(p2))


if __name__ == '__main__':
    unittest.main()
