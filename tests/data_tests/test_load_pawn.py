"""
Testing loading of pawns
"""
import os
import unittest

from game.data.loader import load_pawn


class HappyPath(unittest.TestCase):

    def test_loading_by_name(self):
        goblin = load_pawn("goblin")
        self.assertEqual(goblin.name, "Goblin")

    def test_loading_by_path(self):
        dummy = load_pawn(
            "dummy_pawn",
            f"{os.getcwd()}{os.sep}tests{os.sep}mocks"
        )
        self.assertEqual(dummy.name, "Dummy Pawn")


class CorruptFile(unittest.TestCase):

    # TODO: Invalid references
    # TODO: invalid datatypes
    # TODO: missing required field
    # TODO: wrong file format

    def test_something(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
