"""
Testing loading of skills
"""
import os
import unittest

from game.data.loader import load_skills


class HappyPath(unittest.TestCase):

    def test_loading_by_name(self):
        skill_names = tuple(
            name.split(".")[0]
            for name in os.listdir(f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}skills")
        )
        skills = load_skills(skill_names)
        for name, skill in skills.items():
            self.assertEqual(0, skill.current_duration)  # "useless" assert that checks that everything worked

    def test_loading_by_path(self):
        dummy = load_skills(
            ["dummy_skill"],
            f"{os.getcwd()}{os.sep}tests{os.sep}mocks"
        )["dummy_skill"]
        self.assertEqual(dummy.name, "Dummy Skill")


class CorruptFile(unittest.TestCase):

    # TODO: Invalid references
    # TODO: invalid datatypes
    # TODO: missing required field
    # TODO: wrong file format

    def test_something(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
