"""
Test properties of skills
"""
import unittest

from game.data import Skill


class TestSkillClass(unittest.TestCase):

    def test_damage_changes_when_class_updated(self):
        s1 = Skill.dummy()
        damage1 = s1.damage
        s1.level += 1
        damage2 = s1.damage
        self.assertNotEqual(damage1, damage2, f"{damage1=}, {damage2=}")

    def test_damage_higher_than_base_damage(self):
        s1 = Skill.dummy()
        self.assertTrue(s1.base_damage < s1.damage, f"{s1.base_damage}, {s1.damage}")


if __name__ == '__main__':
    unittest.main()
