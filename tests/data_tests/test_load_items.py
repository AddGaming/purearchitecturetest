"""
Testing loading of items
"""
import unittest
import os

from game.data import Item
from game.data.loader import load_items


class HappyPath(unittest.TestCase):

    def test_loading_by_name(self):
        item_names = tuple(
            name.split(".")[0]
            for name in os.listdir(f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}items")
        )
        items = load_items(item_names)
        for item, count in items.items():
            self.assertEqual(1, count, f"{item} exists {count} times, but should be unique!")

    def test_duplicates_get_counted_correctly(self):
        dummies = load_items(
            ["dummy_item"] * 10,
            f"{os.getcwd()}{os.sep}tests{os.sep}mocks"
        )
        self.assertEqual(dummies[Item("Dummy Item")], 10)
        self.assertEqual(len(dummies), 1)

    def test_loading_by_path(self):
        dummies = load_items(
            ["dummy_item"],
            f"{os.getcwd()}{os.sep}tests{os.sep}mocks"
        )
        self.assertEqual(dummies[Item("Dummy Item")], 1)
        self.assertEqual(len(dummies), 1)


class CorruptFile(unittest.TestCase):

    # TODO: Invalid references
    # TODO: invalid datatypes
    # TODO: missing required field
    # TODO: wrong file format

    def test_something(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
