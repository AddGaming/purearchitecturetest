"""
Testing loading of locations
"""
import unittest
import os

from CLI.factories import load_locations


class HappyPath(unittest.TestCase):

    def test_valid_locations_dont_throw_error(self):
        locations = load_locations(f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}locations")
        for name, location in locations.items():
            self.assertTrue("quit" in location.register)


class CorruptFile(unittest.TestCase):

    # TODO: Invalid references
    # TODO: invalid datatypes
    # TODO: missing required field
    # TODO: wrong file format

    def test_something(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
