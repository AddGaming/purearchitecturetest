"""
Tests the player command 'talk <target>'
"""
import unittest

from CLI import GameState, Settings
from game.data import Pawn, Player
from utils.decorators import in_own_env


class TestDialogSubLoop(unittest.TestCase):

    def test_function_from_default_dict_gets_called_correctly(self):
        self.assertEqual(True, False)

    def test_function_not_in_default_dict_or_specific_prompts_user_again(self):
        self.assertEqual(True, False)


class TestOpeners(unittest.TestCase):

    def test_something(self):
        self.assertEqual(True, True)


class TestDialogBranching(unittest.TestCase):

    @in_own_env
    def test_choosing_by_typing_leads_to_correct_dialog_branch(self, folder: str):
        gs = GameState(
            Player(Pawn.dummy(), ), None, None, None,
            Settings.load_from_file(folder=folder),
            None
        )

        self.assertEqual(True, False)

    def test_choosing_by_number_leads_to_correct_dialog_branch(self):
        self.assertEqual(True, False)


class TestQuestBranch(unittest.TestCase):

    def test_quests_only_visible_once_item_requirement_is_met(self):
        self.assertEqual(True, False)

    def test_quest_only_visible_once_quest_requirement_is_met(self):
        self.assertEqual(True, False)

    def test_repeated_quest_stays_visible(self):
        self.assertEqual(True, False)

    def test_unique_quest_only_visible_the_first_time(self):
        self.assertEqual(True, False)

    def test_if_completion_requirement_not_met_play_not_completed_dialog(self):
        self.assertEqual(True, False)

    def test_if_completion_requirement_met_gives_reward_and_plays_completion_dialog(self):
        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
