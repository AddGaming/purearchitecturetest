"""
tests listing locations
"""
import unittest


class ListLocations(unittest.TestCase):

    def test_normal(self):
        self.assertEqual(True, False)

    def test_locked_locations_not_present(self):
        self.assertEqual(True, False)

    def test_unconnected_locations_not_present(self):
        self.assertEqual(True, False)


if __name__ == "__main__":
    unittest.main()
