"""
tests basic attacks
"""
import unittest


class BasicAttack(unittest.TestCase):

    def test_normal(self):
        self.assertEqual(True, False)

    def test_scores_kill(self):
        self.assertEqual(True, False)

    def test_kill_that_levels_up(self):
        self.assertEqual(True, False)

    def test_buffs_get_accounted_for(self):
        self.assertEqual(True, False)

    def test_player_dies(self):
        self.assertEqual(True, False)

    def test_durations_get_reduced(self):
        self.assertEqual(True, False)

    def test_enemy_fades_due_to_dot(self):  # damage over time
        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
