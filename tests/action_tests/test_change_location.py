"""
test switching locations
"""
import unittest


class ChangeLocation(unittest.TestCase):

    def test_normal(self):
        self.assertEqual(True, False)

    def test_cannot_move_to_non_adjacent_location(self):
        self.assertEqual(True, False)


if __name__ == '__main__':
    unittest.main()
