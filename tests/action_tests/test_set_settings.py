"""
tests basic attacks
"""
import unittest

from CLI import GameState, Settings
from CLI.actions import set_settings
from game.data import Difficulty
from utils.decorators import in_own_env


class ChangingDifficulty(unittest.TestCase):

    # noinspection PyTypeChecker
    @in_own_env
    def test_changing_difficulty_via_integer(self, folder: str):
        gs = GameState(
            None, None, None, None,
            Settings.load_from_file(folder=folder),
            None
        )
        gs.settings.difficulty = Difficulty.EASY
        _, new_gs = set_settings("difficulty", f"{Difficulty.HARD.value}", game_state=gs)
        self.assertEqual(Difficulty.HARD, new_gs.settings.difficulty)

    # noinspection PyTypeChecker
    @in_own_env
    def test_changing_difficulty_via_string(self, folder: str):
        gs = GameState(
            None, None, None, None,
            Settings.load_from_file(folder=folder),
            None
        )
        gs.settings.difficulty = Difficulty.EASY
        _, new_gs = set_settings("difficulty", f"HARD", game_state=gs)
        self.assertEqual(Difficulty.HARD, new_gs.settings.difficulty)


if __name__ == '__main__':
    unittest.main()
