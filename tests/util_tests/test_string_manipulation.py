"""
Tests basic string manipulation
"""
import unittest

from utils import box_in

EXAMPLE_TEXT = f"This\nIs a very creative\nExample text\n"


class TestBoxIn(unittest.TestCase):

    def test_padding_of_internal_text(self):
        new_text = box_in(EXAMPLE_TEXT, indent=0)
        for line in new_text.split("\n")[1:-2]:
            print(line)
            self.assertEqual(line[1], " ")

    def test_consistent_width(self):
        new_text = box_in(EXAMPLE_TEXT, indent=0)
        width = len(new_text.split("\n")[0])
        for line in new_text.split("\n")[:-1]:  # ignore new_line at the end
            self.assertEqual(len(line), width)

    def test_box_indented_1(self):
        new_text = box_in(EXAMPLE_TEXT)
        for line in new_text.split("\n")[:-1]:  # ignore new_line at the end
            self.assertEqual(line[0], "\t")

    def test_box_indented_2(self):
        new_text = box_in(EXAMPLE_TEXT, indent=2)
        for line in new_text.split("\n")[:-1]:  # ignore new_line at the end
            print(line)
            self.assertEqual(line[0], "\t")
            self.assertEqual(line[1], "\t")

    def test_different_box_chars(self):
        alt_text = box_in(EXAMPLE_TEXT, box_char="-").replace("-", "x")
        new_text = box_in(EXAMPLE_TEXT, box_char="x")
        self.assertEqual(new_text, alt_text)

    def test_box_strings_not_allowed(self):
        with self.assertRaises(ValueError):
            box_in(EXAMPLE_TEXT, box_char="He")


if __name__ == '__main__':
    unittest.main()
