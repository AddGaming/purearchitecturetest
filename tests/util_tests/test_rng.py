"""
Tests properties of the rng functions
"""
from time import perf_counter
import unittest

from utils import rng, rng_between


class TestKeyRNGMetrics(unittest.TestCase):

    def test_performance(self):
        # fail if to slow (made for my machine)
        total = 0
        for i in range(100):
            time1 = perf_counter()
            rng(i)
            time2 = perf_counter()
            total += time2 - time1
        print(f"time per hash: {total / 100}s")
        self.assertTrue(total / 100 < 0.00015)

    def test_statistical_distribution(self):
        # fail if in 1000 flips there is no fair distribution
        results = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0}
        for i in range(1000):
            results[rng(i, cut_of=1)] += 1
        print(f"statistical distribution: {results}")
        for key, val in results.items():
            self.assertTrue(val > 80)

    def test_for_chain_repeats(self):
        counter = 0
        prev_result = 0
        highest_rep = 0
        for i in range(1000):
            current_result = rng(i, cut_of=1)
            if prev_result == current_result:
                counter += 1
                if counter > highest_rep:
                    highest_rep = counter
            else:
                counter = 0
            prev_result = current_result

        print(f"{highest_rep =}")
        self.assertTrue(highest_rep < 4)


class TestRNGBetweenMetrics(unittest.TestCase):

    def test_hits_the_whole_range(self):
        results = {5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0}
        for i in range(100):
            results[rng_between(i, 5, 14)] += 1
        for key, val in results.items():
            self.assertTrue(val != 0, f"In the range 0-9, the {key} was never hit")

    def test_statistical_distribution(self):
        # fail if in 1000 flips there is no fair distribution
        results = {5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0}
        for i in range(1000):
            results[rng_between(i, 5, 14)] += 1
        print(f"statistical between distribution: {results}")
        for key, val in results.items():
            self.assertTrue(val > 80)
