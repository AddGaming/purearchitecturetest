"""
Starting Point of the CLI
"""
import os
from CLI import parsing_loop, Settings, load_initial_game_state, create_new_character
from game.data import load_meta
from utils.errors import PawnDoesNotExist

if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))

    settings = Settings.load_from_file()
    meta = load_meta()
    try:
        initial_game_state = load_initial_game_state(settings, meta)
    except PawnDoesNotExist:
        initial_game_state = create_new_character(settings, meta)

    parsing_loop(
        initial_game_state.current_location.register,
        initial_game_state.current_location.name,
        initial_game_state
    )
