# Installing and Running

## Windows:

1. Download the latest release [here](https://gitlab.com/AddGaming/purearchitecturetest/-/releases).
2. Then extract the zip
3. And run the executable

## Linux & Mac:

1. Download or clone the repository.
2. Then install the requirements:
   ```
   python -m pip install requirements.txt
   ```
3. Run the project:
   ```
   python main.py
   ```

# What

This is a small project refactoring my very first game I ever made.
It is a small command line based game which which includes all the typical features and tasks that are common in game
development like:

- reading/writing game data from files
- interactivity
- state change
- calculations with "*random*" output

I wanted to test how far I can take game development with
just [pure functions](https://en.wikipedia.org/wiki/Pure_function).
Meaning all functions should not mutate their input and yield the same output for the same input.

## Why

Since it was my first ever python project, it was pretty terrible.
It included such beautiful code as this function:

```python
def combat():

    global char_lv
    # ... 48 globals later ...
    global char_shield
    skill_2 = 4 + (skillpoints_2 * 2) + skill_4
    skill_3 = 1 + (skillpoints_3 * 2) + skill_4
    skill_5 = 8 + (skillpoints_5 * 3) + (skill_4 * 2)
    skill_6 = 7 + (skillpoints_6 * 2.5) + (skill_4 * 1.5)
    skill_7 = 2 + (skillpoints_7 * 2) + (skill_4 * 2)
    skill_8 = 12 + (skillpoints_8 * 4) + (skill_4 * 3)
    ...
```

This code smells on so many levels, that I just wanted to remake it all, which I did.

# Gameplay Loop

The player has a variety of different activities that he can perform:

- [ ] Explore Towns
    - [x] Talk to NPCs
    - [ ] Craft and upgrade Gear
- [x] Explore Areas
    - [x] Fight Monster
    - [x] Large interconnected Map
- [x] Level in an vast Skill-Tree
- [ ] Questing

## Commands

Since the whole game is command based and played from your favorite terminal,
the player has no visual aide in what actions he can perform.

When starting the game, you will be greeted by something like this:

```
menu $: 
```

This line consists of three elements

```
menu $:
 ^   ^    ^
 |   |  The player input area
 |  Visual seperator
current location
```

Depending on the location of the player, there are different commands available.
While help is always just a `help` away, I list here the most common functions that the player can evoke:

| name     | example    | description                                                                                                                                     |
|----------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| move     | `move inn` | moves the player character to the location with the given name                                                                                  |
| level-up | `level_up` | the player enters the leveling mode and can investigate the current build and invest new skill points into status values and gain new abilities |
| search   | `search`   | the player searches in the current location for monster and items                                                                               |
| help     | `help`     | gives the documentation of all available commands in the given state                                                                            |

For more information, read the Mark Down files provided
for [Monster](https://gitlab.com/AddGaming/purearchitecturetest/-/tree/master/documentation/beastiary), [Items](https://gitlab.com/AddGaming/purearchitecturetest/-/tree/master/documentation/items), [Skills](https://gitlab.com/AddGaming/purearchitecturetest/-/tree/master/documentation/skills),
and [Locations](https://gitlab.com/AddGaming/purearchitecturetest/-/tree/master/documentation/locations).
For a more interactive way of exploring, open the `documentation` *vault* with [Obsidian](https://obsidian.md/) and its
graph view.
For the technical write up, visit [[Design]].