#skill #buffing

## Stats

| name | value |
| -- | -- |
| base damage | 2 |
| damage scaling per lv | 1.5 |
| mana const | 1 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

- [[Acceleration aura]]
- [[Mortal blow]] or [[Impact hammer]] or [[Megaton press]]

## Description

TODO
