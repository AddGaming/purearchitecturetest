#skill #buffing

## Stats

| name | value |
| -- | -- |
| base damage | 2 |
| damage scaling per lv | 1.5 |
| mana const | 1 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

- [[Horizontal slash]]

## Description

increases critical hit chance by 10%

