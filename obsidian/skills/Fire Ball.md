#skill #magical

## Stats

| name | value |
| -- | -- |
| base damage | 3 |
| damage scaling per lv | 2 |
| mana const | 3 |
| cooldown | 3 |
| buffs |  |
| effects |  |

## Requires

- [[Flame]]

## Description

