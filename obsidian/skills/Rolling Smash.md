#skill #physical

## Stats

| name | value |
| -- | -- |
| base damage | 2 |
| damage scaling per lv | 1.5 |
| mana const | 1 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

- [[Power smash]]
- [[Counter attack]]

## Description

Increase Dodge-Chance in this turn to 50% against melee attacks

