#skill #magical

## Stats

| name | value |
| -- | -- |
| base damage | 10 |
| damage scaling per lv | 5 |
| mana const | 20 |
| cooldown | 20 |
| buffs |  |
| effects |  |

## Requires

- [[Fire rune]]

## Description

TODO

