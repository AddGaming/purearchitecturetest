#skill #magical

## Stats

| name | value |
| -- | -- |
| base damage | 1 |
| damage scaling per lv | 1.5 |
| mana const | 1 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

Nothing

## Description

Fundamental Fire-Element magic

