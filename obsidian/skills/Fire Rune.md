#skill #magical

## Stats

| name | value |
| -- | -- |
| base damage | 2 |
| damage scaling per lv | 1.5 |
| mana const | 1 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

- [[Fire ball]]

## Description

Places a fire rune on the enemy that explodes on reactivation.
Damage increases for each turn the spell could charge.

