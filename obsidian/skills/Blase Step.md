#skill #buffing

## Stats

| name | value |
| -- | -- |
| base damage | 2 |
| damage scaling per lv | 1.5 |
| mana const | 0 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

- [[Flame wave]]
- [[Gust storm]]

## Description

TODO
