#skill #physical

## Stats

| name | value |
| -- | -- |
| base damage | 2 |
| damage scaling per lv | 1.5 |
| mana const | 1 |
| cooldown | 1 |
| buffs |  |
| effects |  |

## Requires

- [[Impact smash]]
- [[Double slash]]

## Description

reduces enemy defenses by min(10 , 50%)

