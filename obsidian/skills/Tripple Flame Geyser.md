#skill #magical #high_magic

## Stats

| name | value |
| -- | -- |
| base damage | 30 |
| damage scaling per lv | 15 |
| mana const | 50 |
| cooldown | 40 |
| buffs |  |
| effects |  |

## Requires

- [[Flame geyser]]

## Description

TODO

