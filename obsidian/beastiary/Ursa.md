#monster 

## Stats

| name | value |
| -- | -- |
| hp | 99 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Bear Forest]] (lv. 20-25)

## Lore

TODO