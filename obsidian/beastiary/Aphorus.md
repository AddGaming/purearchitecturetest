#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Tidal Plane]] (lv. 40-50)

## Lore

![[img107616.jpg]]

A small fish that cannot be caught on a hook.

General Attributes

The aphorus is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a fish that is too small to be caught with a hook.