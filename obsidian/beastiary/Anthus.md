#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Cliffted Peaks]] (lv. 10-25)

## Lore

A small bird that fights with horses

![[img107420.jpg]]

General Attributes

The anthus is a small bird that feeds on grass. Since [horses](https://bestiary.ca/beasts/beast212.htm) also feed on grass, they hate the anthus bird for stealing their food. In revenge for this hatred, the bird imitates the neighing of the horse in mockery. The bird is very fertile, producing a dozen eggs at at time.

[Thomas of Cantimpré](https://bestiary.ca/prisources/psdetail1798.htm), in his _Liber de natura rerum_, misidentified the anthus (from [Aristotle](https://bestiary.ca/prisources/psdetail1529.htm), where this story originates) as the _achantis_, some kind of finch, and this error was copied faithfully by later encyclopedists.