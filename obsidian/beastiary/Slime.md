#monster 

## Stats

| name | value |
| -- | -- |
| hp | 200 |
| atk | 100 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |
| [[Dirty Mucus]] | 0.25 |
| [[Small Slime Core]] | 0.75 |

## Locations



## Lore

When left alone for long enough cores start to form inside slimes.
Slimes with a core are one of the strongest opponents you can go up against.
They are overflowing with magical energy making contact with the mucus surrounding the core extremely dangerous.
They can freely manipulate the gell surrounding them change the viscosity.

If one encounters a wild slime, they have two option.
Killing it in one strike from outside their detection range with a piercing projectile,
or running for their lives.