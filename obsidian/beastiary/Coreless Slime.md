#monster 

## Stats

| name | value |
| -- | -- |
| hp | 1 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |
| [[Dirty Mucus]] | 0.2 |

## Locations

- [[Sunshine Hill]] (lv. 1-3)

## Lore

A small slime Monster which core is yet to be formed.
If left alone long enough the core will start materializing and it will become a [[Small Slime]]
In its current status it is almost harmless.

Long skin contact with its mucus create a burning sensation and rashes start to form.
It is advised to use gloves or solid shoes when handling a core-less Slime.