#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Cliffted Peaks]] (lv. 10-15)

## Lore

![[img4470.jpg]]

An animal that is slow and resists commands.

**General Attributes**

Large and tall asses come from Arcadia, but the smaller animals are more useful because they can sustain hardship. Asses are slow and resist commands.

**Illustration**

Asses are usually drawn realistically. In some illustrations the ass is shown as a beast of burden, being ridden, pulling a cart, or walking a treadmill to grind grain.