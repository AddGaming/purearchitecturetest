#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Hill of the Cross]] (lv. 50-70)

## Lore

![[img100754.jpg]]

A bird the color of fire, with razor-sharp wings.

The alerion is a bird like an [eagle](https://bestiary.ca/beasts/beast232.htm); it is lord over all other birds. It is the color of fire, is larger than an eagle, and its wings are as sharp as a razor. There is only one pair in the world. When she is sixty years old, the female lays two eggs, which take sixty days to hatch. When the young are born, the parents, accompanied by a retinue of other birds, fly to the sea, plunge in, and drown. The other birds return to the nest to care for the young alerion until they are old enough to fly. There are similarities in this story to that of the [Phoenix](https://bestiary.ca/beasts/beast149.htm).

This tale is told in the _Bestiaire_ of [Pierre de Beauvais](https://bestiary.ca/prisources/psdetail1886.htm); a similar tale is found in some versions of The Letter of Prester John on the marvels of the east.