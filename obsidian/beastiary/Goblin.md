#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |
| [[Stick]] | 0.2857142857142857 |
| [[Ripped Cloth]] | 0.14285714285714285 |
| [[Brittle Wooden Club]] | 0.07142857142857142 |

## Locations

- [[Rust Cave]] (lv. 50-60)
- [[Sunshine Forest]] (lv. 5-20)
- [[Bear Forest]] (lv. 10-20)

## Lore

