#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Thousand Snake Swamp]] (lv. 50-70)

## Lore

![[img100487.jpg]]

Blocks its ear with its tail so as not to hear the charmer.

**General Attributes**

The asp is a serpent that avoids the enchantment of music by pressing one ear against the ground and plugging the other ear with its tail. In some versions the asp guards a tree that drips balm; to get the balm men must first put the asp to sleep by playing or singing to it. Another version holds that the asp has a precious stone called a [carbuncle](https://bestiary.ca/beasts/beast1228.htm) in its head, and the enchanter must say certain words to the asp to obtain the stone.

The Haemorrhois, Prester and Hypnalis are other varieties of asp. As noted in the [Aberdeen Bestiary](https://bestiary.ca/manuscripts/manu100.htm): "The emorrosis is an asp, so called because it kills by making you sweat blood. If you are bitten by it, you grow weak, so that your veins open and your life is drawn forth in your blood. For the Greek word for 'blood' is _emath_. ... The prester is an asp that moves quickly with its mouth always open and emitting vapour... If it strikes you, you swell up and die of gross distention, for the swollen body putrefies immediately after... There is a kind of asp called ypnalis, because it kills you by sending you to sleep. It was this snake that Cleopatra applied to herself, and was released by death as if by sleep."

**Allegory/Moral**

The asp represents the worldly and wealthy, who keep one ear pressed to earthly desire, and whose other ear is blocked by sin.

**Illustration**

The usual illustration of the asp shows the enchanter reading from a scroll or playing a musical instrument, with the snake at his feet blocking its ears. As second type of illustration shows the enchanter touching or hitting the asp's head with a stick or a wand. The asp guarding the balsam tree is occasionally illustrated.

For the illustration of the asp in [British Library, Harley MS 4751](https://bestiary.ca/manuscripts/manu1010.htm), folio 61r, the scroll the enchanter is reading from has writing on it. The writing says _Super aspidem et basiliscum amb._; the full quote is _Super aspidem et basiliscum ambulabis, et conculcabis leonem et draconem_ (Upon the asp and [basilisk](https://bestiary.ca/beasts/beast265.htm) shalt thou tread, and thou shalt trample upon the [lion](https://bestiary.ca/beasts/beast78.htm) and [dragon](https://bestiary.ca/beasts/beast262.htm)). This is from the Christian bible, Psalm 91, verse 13. This verse was used in a [monastic chant](https://cantus.uwaterloo.ca/chant/229344), which suggests the man is singing the words to enchant the asp. In [Kongelige Bibliotek, GKS 1633 4°](https://bestiary.ca/manuscripts/manu94.htm) (folio 52r) the enchanter also has a scroll with two words on it (_canta serpens_?). In [Museum Meermanno, MMW, 10 B 25](https://bestiary.ca/manuscripts/manu2002.htm) (folio 41r) there is a scroll with illegible writing on it. In other manuscripts the enchanter has a book or other text with no words visible.