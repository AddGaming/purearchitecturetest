#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Forest of Wavering Mist]] (lv. 20-30)
- [[Bear Forest]] (lv. 15-25)

## Lore

![[img107187.jpg]]

An animal as large as a stag

The ahane is an animal about the size of a [stag](https://bestiary.ca/beasts/beast162.htm), but is different from other animals; every four-footed animal has a gallbladder in its interior belly, with the exception of this one; this animal has its gallbladder in its ears.