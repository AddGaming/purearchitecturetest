#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Caldera Lake]] (lv. 40-45)

## Lore

![[img107617.jpg]]

A fish that appears when the Pleiades decline.

**General Attributes**

The australis is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm).The australis or "southern fish" appears when the Peiades begin to decline in the west. It is not given a physical description.