#monster 

## Stats

| name | value |
| -- | -- |
| hp | 99 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |
| [[Small Slime Core]] | 0.2 |
| [[Dirty Mucus]] | 0.8 |

## Locations

- [[Sunshine Hill]] (lv. 45-55)

## Lore

A small variant of a [[Slime]].