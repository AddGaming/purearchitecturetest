#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Caldera Lake]] (lv. 5-10)
- [[Lake Talpha]] (lv. 10-20)

## Lore

![[img108395.jpg]]

A fish used to make pickled fish sauce.

The allec is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm).It was used by the Romans to make a pickled fish sauce (_salsamentum_). It is very small, but when it is freshly caught, it makes the most delicious food. When salted it can last longer than other fish used by humans. It eyes shine at night, and it can be lured into nets by light.

Reality

_Allec_, _hallec_, _garum_ and _liquamen_ were the names of a Roman fish sauce. "Pliny stated that garum was made from fish intestines, with salt, creating a liquor, the garum, and the fish paste named (h)allec or allex; this paste was a byproduct of fish sauce production. A concentrated garum evaporated down to a thick paste with salt crystals was called muria; it would have been used to salt and flavor foods. Garum was produced in various grades consumed by all social classes. After the liquid was ladled off of the top of the mixture, the remains of the fish, called allec, was used by the poorest classes to flavour their staple porridge." - [[Wikipedia](https://en.wikipedia.org/wiki/Garum)]

Allec may be the herring.