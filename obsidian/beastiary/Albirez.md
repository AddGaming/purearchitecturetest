#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Tidal Plane]] (lv. 90-110)

## Lore

![[img107605.jpg]]

A fish with a very hard skin.

The albirez is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a fish with a thick and very hard skin, so it can survive strong blows. It is said Roman soldiers would form the skin into a cap to wear under their helmets so they would not feel pain when hit.