#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Lake Talpha]] (lv. 5-15)
- [[Thousand Snake Swamp]] (lv. 10-30)

## Lore

![[img107600.jpg]]

A fish the is generated from rotting mud.

General Attributes

The alphoraz is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is not born, but instead is generated spontaneously from rotting mud. It does not live long, soon decaying in the water.

[Thomas of Cantimpré's](https://bestiary.ca/prisources/psdetail1798.htm) description of the alphoraz closely matches [Aristotle's](https://bestiary.ca/prisources/psdetail1529.htm) description (Book 6, chapter 15.1-2) of the [eel](https://bestiary.ca/beasts/beast106039.htm).