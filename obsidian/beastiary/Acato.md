#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Boatwright Bay]] (lv. 1-1)
- [[Volupta Beach]] (lv. 1-1)

## Lore

*No Image*

Can be used to find pearls.

Agate is a stone that is used to find [pearls](https://bestiary.ca/beasts/beast548.htm). When divers want to locate pearls, they tie an agate to a rope and drop it into the sea. The agate is attracted to a pearl, and the diver can follow the rope to where the pearl lies.