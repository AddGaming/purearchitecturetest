#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations



## Lore

![[img4443.jpg]]

Called "simia" because it is similar to humans.

**General Attributes**

The female ape always gives birth to twins, one of which she loves and the other she hates. When she carries her young, she holds the one she loves in her arms, but the one she hates must cling to her back. When the ape is pursued by a hunter, she tires from running while carrying her two children; when she is in danger of being caught, she drops the child she loves in order to escape, but the one she hates continues to cling to her back and is saved.

There are five types of apes. The first is called _cericopithicus_ and has a tail. The second, with rough hair, is called _sphinx,_ and is docile, not wild. The third is _cynocephalus_, with a head like a dog and a long tail. The fourth, the [satyr](https://bestiary.ca/beasts/beast101074.htm) (_satyrus_, described separately here), is lively and has a pleasant face. The fifth, called _callitrix_, has a long beard on its pointed face, and has a wide tail.

Apes are happy at the new moon but grow sad as it wanes. At the equinox they urinate seven times. Apes are said to be ugly, dirty beasts with flat and wrinkled noses; their rear parts are particularly horrible.

**Allegory/Moral**

The ape is usually equated with the devil. It is said, that just as apes have a head but no tail, Satan began as an angel in heaven, but "he lost his tail, because he will perish totally at the end". In [Philippe de Thaon's](https://bestiary.ca/prisources/psdetail889.htm) _Bestiaire_ it is said that the devil mocks evil-doers and will carry them in front of him to hell, while the good remain behind his back with God.

**Illustration**

Most illustrations show the mother ape with two children, one held in front and the other clinging to her back. The story of the ape trying on boots is also sometimes illustrated. Apes are often used in marginal illustrations in manuscripts, where they are shown imitating some human behavior; common scenes show the ape as a knight riding a horse, or examining a flask of urine in mockery of a medieval physician. Apes are occasionally shown eating an apple; they were thus associated with the sin of Eve and Adam in eating the forbidden fruit, usually said to be an apple.