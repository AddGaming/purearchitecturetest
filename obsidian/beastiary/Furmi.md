#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Plane of the Red Pillars]] (lv. 20-20)

## Lore

### Game Lore

  
  
  

### Original Lore

![[img100567.jpg]]

  

Ants harvest grain to store for the winter.

  

**General Attributes**

  

Ants are said to have these characteristics: they walk in order like soldiers; they carry grains in their mouths, and an ant with no grain will not try to take the grain from one which has it; they break each grain in half to keep it from germinating when it rains, because if it does the ants will starve in the winter; when it is time to harvest the grain, they go into the fields and climb up to the grain, where they distinguish wheat from barley by its smell and reject the barley because it is food for cattle. If their grain gets wet in the rain, they wait for a sunny day to bring the seeds out of their nest to dry, then return it to their stores.

  

Some authors tell of the gold-digging ants of Ethiopia, which are the size of dogs. These ants dig up gold from sand with their feet and guard it, chasing down and killing any who try to steal it. It is said that people safely steal the ant's gold by separating mares from their foals, with a river between them. The mares, carrying packs, are driven to the side of the river inhabited by the ants; the ants, seeing the packs as a good place to hide their gold, fill them with the golden sand. When the mares swim back to their foals on the other side of the river, the ants cannot follow. This story is illustrated in the _Image du Monde_ by [Gossuin de Metz](https://bestiary.ca/prisources/psdetail104983.htm) (manuscript [Bibliothèque de la Faculté de Médecine, H. 437](https://bestiary.ca/manuscripts/manu1521.htm), folio 104v), the _Bestiaire_ of [Guillaume le Clerc](https://bestiary.ca/prisources/psdetail1097.htm) ([Bibliothèque Nationale de France, fr. 14969](https://bestiary.ca/manuscripts/manu1534.htm), folio 17r), and the Queen Queen Psalter ([British Library, Royal MS 2 B VII](https://bestiary.ca/manuscripts/manu973.htm), folio 96r).

  

**Allegory/Moral**

  

The ants working together for the common good is to be taken as a lesson to men, who should work in unity.

  

The splitting of the grain represents the separation that must be made in the interpretation of the Bible, distinguishing the literal from the spiritual meaning, "lest the law interpreted literally should kill you". Some sources compare the ants to the Jews, who have taken the law literally and have "died of hunger".

  

The barley the ants reject signifies the heresy that Christians are to cast away.

  

**Illustration**

  

The bestiary illustrations of ants are almost always poor, showing merely a series of dots or bean-shaped objects with legs. The most common images are of ants walking in lines toward grain or climbing grain stalks to gather the seeds.