#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Caldera Lake]] (lv. 3-6)

## Lore

![[img108394.jpg]]

A fish spontaneously generated from the foam of rain water.

**General Attributes**

The astaraz is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a fish that is generated from the foam caused by rain near the shore. They cannot stand the brightness of the sun, so they find trees overhanging the water where they can have shade.
