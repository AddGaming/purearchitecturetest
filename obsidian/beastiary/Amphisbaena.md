#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Thousand Snake Castle]] (lv. 50-100)

## Lore

![[img100489.jpg]]

A serpent with two heads, one at either end.

**General Attributes**

The amphisbaena is a two-headed lizard or serpent. It has one head in the normal position, and another at the end of its tail. It can therefore run in either direction. Its eyes shine like lamps, and it has no fear of cold.

**Illustration**

The amphisbaena is usually depicted as a dragon-like beast having wings and two feet, with horns on its head. There is always a second head on the end of its tail, usually smaller than the other head, which is attached to the front of a body. In some illustrations there is no distiction between head and tail; the amphisbaena is depicted as a snake with an equal-sized head at either end, and only a thin snake body between. In many cases the "primary" head is shown biting the neck of the head on the tail.

**Reality**

The name "amphisbaena" is now given to a legless lizard that can move either forward or backward, though this is a relatively modern use of the name.

