#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Yofel Castle]] (lv. 40-50)

## Lore

![[img101243.jpg]]

The offspring of an ant and a lion.

**General Attributes**

There are two interpretations of what an ant-lion is. In one version, the ant-lion is so called because it is the "lion of ants," a large ant or small animal that hides in the dust and kills ants. In the other version, it is a beast that is the result of a mating between a [lion](https://bestiary.ca/beasts/beast78.htm) and an [ant](https://bestiary.ca/beasts/beast218.htm). It has the face of a lion and and the body of an ant, with each part having its appropriate nature. Because the lion part will only eat meat and the ant part can only digest grain, the ant-lion starves.

The ant-lion story may come from a mistranslation of a word in the Septuagint version of the biblical Old Testament, from the book of Job (4:11). The word in Hebrew is _lajisch_, an uncommon word for lion, which in other translations of Job is rendered as either lion or tiger; in the Septuagint it is translated as _mermecolion_, ant-lion.

**Illustration**

Images of the ant-lion are rare. They show either a large ant or hybrid beast, half lion and half ant.