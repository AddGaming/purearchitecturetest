#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Monument of Swordsmen]] (lv. 100-120)

## Lore

![[img4467.jpg]]

An animal so wild no hunter can approach it.

**General Attributes**

The antelope is so wild that hunters cannot catch it, except in one instance: When the antelope is thirsty it goes to the Euphrates River to drink, but as it plays in the thickets of herecine trees there, its horns get caught in the branches and it cannot free itself. The hunter, hearing its cries, comes and kills it.

Its horns are like saws, and with them it can cut down trees.

**Allegory/Moral**

The antelope's two horns represent the biblical Old and New Testaments, with which people can cut themselves free of vice. People are also warned not to play in the "thickets of worldliness" where pleasure can kill body and soul.

**Illustration**

Antelope illustrations vary considerably, depicting the animal as anything from dog-like to horse-like. They are almost always given horns; in some cases the saw-like nature of the horns is minimal, while in others it is greatly exaggerated. The antelope is almost always shown with its horns tangled in a bush or tree, usually with a man killing it with a spear or an axe. Sometimes the river the antelope is said to drink from is also shown.

**Heraldry**

The antelope was not much used in heraldry, though the antelope head alone was more common. On his badge King Henry IV used a white antelope with gold horns, tusks and hooves. Kings Henry V and VI also used the antelope. It is thought that this beast was the badge of the Bohun family in England.

**Reality**

The antelope of the [_Physiologus_](https://bestiary.ca/prisources/psdetail869.htm) and the bestiaries is not the animal now called antelope. Medieval writers were unsure of its identity, hence its many names.