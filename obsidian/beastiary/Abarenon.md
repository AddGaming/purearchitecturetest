#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Boatwright Bay]] (lv. 20-50)
- [[Volupta Beach]] (lv. 20-50)

## Lore

![[img107610.jpg]]

A fish that lays eggs by rubbing on sand.

The Aberenon is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a fertile fish that lays many eggs. To lay its eggs it rubs its belly on rough sand on the ocean bottom.