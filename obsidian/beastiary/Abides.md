#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Thousand Snake Swamp]] (lv. 30-50)
- [[Thousand Snake Castle]] (lv. 40-60)

## Lore

![[img107501.jpg]]

A marine animal that first lives in the water and then on land.

The abides is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a marine animal that first lives the the water, but then its form and character change so that it can live on land.

TODO: Split into jung and mature form