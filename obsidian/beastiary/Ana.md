#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Fields of Murrayfield]] (lv. 120-130)

## Lore

![[img108157.jpg]]

A beast that comes to the defense of others of its own kind.

General Attributes

The ana appears in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is an animal that lives in the East. It has long teeth with which it attacks other animals. If the ana sees one of its kind being approached by any other kind of animal, all the ana that are near gather to attack the other animal, no matter what kind it is.