#monster 

## Stats

| name | value |
| -- | -- |
| hp | 1 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Fields of Murrayfield]] (lv. 80-90)

## Lore

![[img107347.jpg]]

A bird seldom seen because it flies above the clouds.

The aeriophylon is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). Aeriophylon is a Latin and Greek name, derived from 'air' and 'phylos' (which is love), as if 'lover of air'. It is seldom seen by people because it flies above the clouds, rarely coming to land. If it is captured while still young, it can be trained as a hunter, and will stay with the person who raised it without being confined.