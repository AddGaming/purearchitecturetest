#monster 

## Stats

| name | value |
| -- | -- |
| hp | 10 |
| atk | 1 |
| def | 0 |
| magic def | 0 |
| mana | 0 |
| mana_regeneration | 0 |
| luck | 0 |
| speed | 1 |

## Drops

| name | rate |
| -- | -- |

## Locations

- [[Lake Talpha]] (lv. 20-30)

## Lore

![[img107612.jpg]]

A fish with purple or red markings.

General Attributes

The amia is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm).It is a rock fish with purple stripes on its sides. [Thomas of Cantimpré](https://bestiary.ca/prisources/psdetail1798.htm) says that it has a stone inside, and that its skin has multiple colors like silk cloth.

Reality

The amia may be the bonito, bluefish or mackerel.