#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A serpent that is more cunning than any other

![[img107745.jpg]]

### General Attributes

The berus is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a wicked serpent, more cunning and deceitful than any other serpent. It tricks the sea [moray](https://bestiary.ca/beasts/beast106039.htm) (_murenam marinam_) and then kills it.