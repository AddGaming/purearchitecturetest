#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A fish with a beard

![[img106036.jpg]]

### General Attributes

The barbulus fish gets its name because it has a beard (_barba_).

### Reality

There are several fish species related to _barbulus_, found in rivers in Europe.