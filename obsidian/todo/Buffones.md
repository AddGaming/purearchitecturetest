#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

An insect that has a voice like a trumpet

![[img107804.jpg]]

### General Attributes

The buffones is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a worm (insect) that is poisonous and foul to the touch. It only eats earth, and only as much in a day as it can hold in a forefoot. There is a species of buffones in Gaul (France) that has a voice like a trumpet; elsewhere they are mute.

It is not at all clear what this "worm" (or reptile, as some authors claim) actually is. Its physical appearance is not described (other than its color), and what little is said of it is inconsistent and confusing. Buffones can also mean toad, and as in the word "buffoon", a clown or joker. It is likely that [Thomas of Cantimpré](https://bestiary.ca/prisources/psdetail1798.htm) confused the descriptions of one or more of his (unknown) sources to come up with the buffones.

It is possible that the names buffones and [botrax](https://bestiary.ca/beasts/beast106209.htm) (also called _borax_) both refer to the [frog](https://bestiary.ca/beasts/beast537.htm) or toad (_bufo_), which is said to have a stone in its forehead called the toadstone, which [Thomas of Cantimpré](https://bestiary.ca/prisources/psdetail1798.htm) says is called _crapadina_ in French. This stone is a remedy against poison. Thomas's description of the buffones says they call out it turn two by two, which suggests the call and response of a pair of toads or frogs. Thomas says the botrax prefers wet place, and also says it is a species of buffones. These corresponences suggest that buffones and botrax refer to toads or frogs, but this speculative and uncertain.