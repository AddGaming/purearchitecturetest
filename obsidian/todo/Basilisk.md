#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

Its odor, voice and even look can kill

![[img100484.jpg]]

### General Attributes

The basilisk is usually described as a crested [snake](https://bestiary.ca/beasts/beast264.htm), and sometimes as a [cock](https://bestiary.ca/beasts/beast258.htm) with a snake's tail. It is called the king _(regulus)_ of the serpents because its Greek name _basiliscus_ means "little king"; its odor is said to kill snakes. Fire coming from the basilisk's mouth kills birds, and its glance will kill a man. It can kill by hissing, which is why it is also called the _sibilus_. Like the [scorpion](https://bestiary.ca/beasts/beast281.htm) it likes dry places; its bite causes the victim to become hydrophobic. A basilisk is hatched by a toad from a cock's egg, a rare occurrence. Only the [weasel](https://bestiary.ca/beasts/beast150.htm) can kill a basilisk.

Some manuscripts have separate entries and/or illustrations for the basilisk and the regulus, possibly because the basilisk account in [Isidore](https://bestiary.ca/prisources/psdetail821.htm) has three sections, one each for the _basilisk_, the "kinglet" _(reguli)_, and the _sibilus_. Where the regulus is treated separately, the bite of the basilisk causing hydrophobia is generally ascribed to the regulus.

### Illustration

Basilisk illustrations vary greatly. Most show a bird-snake hybrid, with the beast being a bird (often a cock) with a snake tail. In some images the basilisk is all snake with no bird parts. The basilisk as king of the serpents appears in some manuscripts, with the basilisk wearing a crown. The weasel attacking the basilisk is commonly included in the image.