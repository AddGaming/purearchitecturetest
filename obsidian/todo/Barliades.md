#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A bird that initially grows from trees

![[img101954.jpg]]

### General Attributes

Barnacle geese come from trees that grow over water. These trees produce birds that look like small geese; the young birds hang from their beaks from the trees. When the birds are mature enough, they fall from the trees; any that fall into the water float and are safe, but those that fall on land die.

An alternate version has it that barnacle geese appear attached to logs drifting in the sea. After they grow feathers, they swim away as geese.

There was considerable debate by both Christians and Jews as to how to treat barnacle geese under religious dietary rules. If they grew on trees but became birds, were they to considered to be plants or animals? For Christians, could they be eaten on days when eating meat was forbidden? At the General Lateran Council of 1215, Pope Innocent III declared that "they would henceforth be banned. These are birds, for they eat herbs and grasses (like geese), live on dry land, differ in no way". For Jews, it was a question of whether they needed to be killed as fruit, fish or flesh before being eaten, according to Jewish law. In the twelfth century Rabbi Jacob Thom of Ramerii (died 1171) decided that they must be killed as flesh.

### Illustration

Barnacle geese are almost always shown as birds hanging from trees by their beaks, sometimes with mature birds dropping into the water.

### Reality

The Barnacle Goose (Branta leucopsis) is a real bird that is mostly found in northern Europe. Because the birds migrate north to breed, Europeans did not see their nests, eggs or chicks when other geese were breeding, and so assumed they did not breed like normal geese. If they did not breed, they must come from spontaneous generation, as told by Gerald of Wales, being formed on drifting logs before maturing as geese. The descriptions suggest that the "seeds" of the geese were actually [Goose barnacles](https://en.wikipedia.org/wiki/Goose_barnacle), which vaguely resemble Barnacle Geese in color pattern. Oddly, the chicks of these geese do drop from a height to reach water; the adults nest on cliffs above the ocean, and the [chicks jump off](https://www.wwt.org.uk/news-and-stories/blog/barmy-barnacle-geese/) soon after they hatch.