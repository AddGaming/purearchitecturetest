#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A slow moving carrion eating bird

![[img107352.jpg]]

### General Attributes

The bustard (_bistarda_) has its name because it is slow (_tarda_); it cannot ascend rapidly like other birds because of its heavy flight. It has to leap two or three times before it gets into the air. It does not take its prey in the air, but if it sees an animal on the ground it will join with others of its kind to kill and eat it. It also eats carrion, and even grass.