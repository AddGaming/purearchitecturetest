#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A bird that gives birth to living young

![[img365.jpg]]

### General Attributes

A bat is not a noble bird. It is unlike other birds in that it gives birth to live young instead of laying eggs, and it has teeth. Bats gather together and hang from high places like a bunch of grapes; if one falls, all the rest also fall.