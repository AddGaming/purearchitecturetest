#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

Hunted for its testicles, it castrates itself to escape from the hunter

![[img4461.jpg]]

### General Attributes

The beaver is hunted for its testicles, which are valued for making medicine. When the beaver sees that it cannot escape from the hunter, it bites off its testicles and throws them to the hunter, who then stops pursuing the beaver. If another hunter chases the beaver, it shows the hunter that it has already lost its testicles and so is spared.

### Allegory/Moral

If a man wishes to live chastely he must cut off all his vices and throw them from him into the face of the devil. The devil, seeing that the man has nothing belonging to him, will leave the man alone.