#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A dirty beast that bites

![[img101915.jpg]]

### General Attributes

Badgers work together to dig their holes in the mountains. One will lie down at the entrance to the hole, holding a stick in its mouth, while the others pile earth on its belly. Two badgers take hold of the stick with their mouths and drag the loaded badger away.

Badgers love honey and constantly look for honeycombs.

### Illustration

The usual illustration shows a badger lying on its back, with two badgers gripping the stick in its mouth and pulling, while others dig the hole and pile up dirt.