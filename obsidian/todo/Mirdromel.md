#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A bird that makes a booming noise

![[img5183.jpg]]

### General Attributes

The bittern is a bird that puts its beak in the water or into reeds and makes a booming noise, which can be heard for two leagues at dawn.