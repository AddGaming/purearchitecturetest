#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A savage wild pig or hog

![[img101903.jpg]]

### General Attributes

A savage wild pig or hog. The Jews consider this animal to be unclean and will not eat its flesh.

See also [Pig (Domestic)](https://bestiary.ca/beasts/beast102115.htm).

### Allegory/Moral

The boar signifies the fierceness of the the world's rulers.

### Illustration

The boar is quite accurately drawn on most manuscripts. An illustration found in several manuscripts shows two boars in profile, one in the background and the other in the foreground and frequenly overlapping, and usually facing in opposite directions.