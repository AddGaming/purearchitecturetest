#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore
A fish that lives in caves near Babylon

![[img107623.jpg]]

### General Attributes

The Babylonian Fish is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It lives near Babylon, where rivers fall into caves. It has a head like a [sea-frog](https://bestiary.ca/beasts/beast107688.htm), and it swims with frequent movements of its fins and tails.