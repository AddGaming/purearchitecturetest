#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

Buffalo are so wild that they cannot be yoked

![[img102100.jpg]]

### General Attributes

The buffalo is similar to the [ox](https://bestiary.ca/beasts/beast199.htm) but is too wild to be yoked.

There are several names used for the buffalo in medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). The animal referred to has a variety of characteristics; some are common to all or most of the accounts, but there are differences.

- The _aurochs_ is a kind of buffalo from Germany; it has huge horns that are made into drinking vessels for kings.
- The _bubalus_ is an animal bigger and taller than an [ox](https://bestiary.ca/beasts/beast199.htm). It has long and crooked horns, a long neck, a huge head, and a kind and simple appearance, but it is extremely destructive and cruel when provoked to anger.
- The _urin_ (_uri_) is an animal with horns like those of a [bull](https://bestiary.ca/beasts/beast198.htm), which grow so large they are used for the goblets of kings among the royal tables.
- The _vesontes_ is a beast like an ox, with bristles on its neck and a mane like a [horse](https://bestiary.ca/beasts/beast212.htm). It is is so fierce that it can never be tamed or controlled when captured by men
- The _zubrones_ is a kind of wild [bull](https://bestiary.ca/beasts/beast198.htm) that lives in Bohemia. It is said to to be largest of the wild bulls, with huge horns that are made into drinking vessels. When it attacks a man, it catches him on its horns and tosses him into the air, eventually killing him. It is so fast that it can eject its dung, turn and catch it on its horns, and throw it at a pursuer.

The use of dung as a weapon is most commonly found in accounts of the [bonnacon](https://bestiary.ca/beasts/beast80.htm), which appears in bestiaries as well as encyclopedias.

### Allegory/Moral

The buffalo represents overly proud teachers and those that lord it over the people. Such teachers choose to rule over the clergy instead of leading them as a flock.