#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

An enormous four-footed beast

![[img104449.jpg]]

### General Attributes

In Jewish legend the Behemoth is an unconquerable land monster that lives in a desert east of the Garden of Eden; some sources say it lives in the mountains. It is usually paired with the [Leviathan](https://bestiary.ca/beasts/beast104446.htm), another monster that lives in the sea. They were created at the same time, but were separated because no one place would be enough for both. At the end of time there will be a battle between the two monsters, and God will slay them both with a sword and the elect will eat them. The Behemoth does not appear in bestiaries, but it is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm).

### Illustration

The Behemoth is usually illustrated as a four footed land animal with horns and multiple tusks. It is shown being ridden by a demon (or perhaps Satan).