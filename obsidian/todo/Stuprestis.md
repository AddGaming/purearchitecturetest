#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A venomous beetle that causes cows to burst

![[img107849.jpg]]

### General Attributes

The buprestis is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a venomous beetle from Italy. It hides in the grass, and when [cows](https://bestiary.ca/beasts/beast102116.htm) eat it the poison causes them to swell up and burst.