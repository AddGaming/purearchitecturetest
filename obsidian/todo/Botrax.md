#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A serpent with the face of a frog

![[img107806.jpg]]

### General Attributes

The botrax is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a serpent with the face of a [frog](https://bestiary.ca/beasts/beast537.htm). It has a stone on its forehed that is used as a remedy against poison. It likes to live in wet places. It can be defeated by the [spider](https://bestiary.ca/beasts/beast551.htm), which bites it and causes it to swell.

The botrax and the buffones may both be the same animal, the toad or the [frog](https://bestiary.ca/beasts/beast537.htm). See the [buffones](https://bestiary.ca/beasts/beast106208.htm) entry for more on this.