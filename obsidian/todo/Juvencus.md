#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

The Indian bull is tawny colored and swift as a bird

![[img2196.jpg]]

### General Attributes

The Indian bull has horns that are movable, and a hide so hard it rejects spears. Its tawny colored hair turns contrariwise, and it is swift as a bird. If captured it breathes fiercely to avoid being tamed. Gentiles sacrifice the young bull (bullock) rather than the old bull.

See also [Cow (Domestic)](https://bestiary.ca/beasts/beast102116.htm).