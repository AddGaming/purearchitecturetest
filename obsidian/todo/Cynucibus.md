#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A blood sucking worm that preys on humans

![[img107819.jpg]]

### General Attributes

The bedbug is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It has its name from the similarity of its smell to that of an herb (_cimicia_).

### Reality

In modern taxonomy, _Cimex_ is a genus of blood sucking insects (including bedbugs) in the _Cimicidae_ family.