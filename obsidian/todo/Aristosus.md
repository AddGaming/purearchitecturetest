#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A fish that has so many many bones that it is difficult to eat

![[img107723.jpg]]

### General Attributes

The bitterling is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a fish that is so full of bones that it is difficult to eat, and the taste is so unpalatable that it is only used as food by the poor. The fish is caught in nets: an arch containing a small bell is floated in from of the net, and the fish follow the tinkling sound into the net.