#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

The cubs are born unformed, and are licked into shape by the mother

![[img100531.jpg]]

### General Attributes

Bears give birth in the winter. The bear cub is born as a shapeless and eyeless lump of flesh, which the mother bear shapes into its proper form by licking it (the origin of the expression "to lick into shape"). The cub is born head first, making its head weak and its arms and legs strong, allowing bears to stand upright. Bears do not mate like other animals; like humans they embrace each other when they copulate. Their desire is aroused in winter. The males do not touch the pregnant females, and even when they share the same lair at the time of birth, they lie separated by a trench. When in their fourteen day period of hibernation, bears are so soundly asleep that not even wounds can wake them. Bears eat honey, but can only safely eat the apples of the [mandrake](https://bestiary.ca/beasts/beast1098.htm) if they also eat [ants](https://bestiary.ca/beasts/beast218.htm). Bears fight [bulls](https://bestiary.ca/beasts/beast198.htm) by holding their horns and attacking their sensitive noses. If injured, a bear can heal itself by touching the herb phlome or mullein. The fiercest bears are found in Numibia.