#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

Bees are the smallest of birds, and are born from the bodies of oxen

![[img100478.jpg]]

### General Attributes

Bees are the smallest of birds. They are born from the bodies of oxen, or from the decaying flesh of slaughtered calves; worms form in the flesh and then turn into bees. Bees live in community, choose the most noble among them as king, have wars, and make honey. Their laws are based on custom, but the king does not enforce the law; rather the lawbreakers punish themselves by stinging themselves to death. Bees are industrious and hard working; they will never fail to leave the hive to work on suitable days. They have everything in common, and even raise their young communally. Bees are afraid of smoke and are excited by noise. Each has its own duty: guarding the food supply, watching for rain, collecting dew to make honey, and making wax from flowers.

Drones (_fucus_) are larger bees that eat what is provided by others. _Cortri_ are large bees that some say are the kings.

Medieval writers had a great deal to say about bees; their attributes were a excellent basis for moralizations and allegory. In many cases the chapter on bees is several pages long, both in [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm) and in bestiaries. The writers had many earlier sources to draw on, including [Aristotle](https://bestiary.ca/prisources/psdetail1529.htm), [Pliny the Elder](https://bestiary.ca/prisources/psdetail529.htm), [Claudius Aelianus](https://bestiary.ca/prisources/psdetail1495.htm), [Saint Ambrose](https://bestiary.ca/prisources/psdetail815.htm), [Isidore of Seville](https://bestiary.ca/prisources/psdetail821.htm) and others, and most of these also had a great deal to say about bees.

In manuscripts, bees are variously categorized as birds, insects and worms.