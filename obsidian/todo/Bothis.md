#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A flat fish that goes to the river bottom and muddies the water to hide

![[img107619.jpg]]

### General Attributes

The bothis is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a flat river fish with a large body. When hunted by fishermen it goes to the bottom and muddies the water above it so it cannot be seen. Its back is similar in appearance to the bottom mud, which also helps it hide. It has red spots on its back. It is impregnated by the south wind.

There is a similarly named fish, the [borbothis](https://bestiary.ca/beasts/beast107620.htm), which is described entirely differently. The relation between them, if any, is unknown.

### Reality

The bothis is probably one of the flatfish, like the flounder, halibut or sole