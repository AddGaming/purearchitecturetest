#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

Bee-eater young care for their elderly parents

![[img105986.jpg]]

### General Attributes

This bird called _meropes_ in manuscripts has been tentatively identified as the bee-eater. The texts say it is affectionate to its parents, and helps rejuvinate them by licking the mist from their eyes and pulling out old feathers. This is also said of the [hoopoe](https://bestiary.ca/beasts/beast243.htm). This bird appears in the _Liber Floridus_, an [encyclopedia](https://bestiary.ca/prisources/psdetail105102.htm) by [Lambert of Saint-Omer](https://bestiary.ca/prisources/psdetail4892.htm) and in other encyclopedias, and in at least one bestiary ([Cambridge University Library, Kk. 4. 25](https://bestiary.ca/manuscripts/manu946.htm), folio 84v).

### Reality

The modern genus name for the European Bee-eater is _merops_.