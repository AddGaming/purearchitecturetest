#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

An insect that generates small worms from its dung

![[img106792.jpg]]

### General Attributes

The butterfly is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a small flying animal that causes small worms to be generated from its dung.