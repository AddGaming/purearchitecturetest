#item

## Stats

| name | value |
|------|-------|
| x    | 1     |

## Dropped from

| name | chance |
|------|--------|
| x    | 1      |

## Found in

- TODO

## Lore

Insects that are generated in wine

### General Attributes

Bibliones are insects that are generated in wine.