#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A beast like a bull, that uses its dung as a weapon

![[img155.jpg]]

### General Attributes

The bonnacon is a beast with a head like a bull, but with horns that curl in towards each other. Because these horns are useless for defence, the bonnacon has another weapon. When pursued, the beast expels its dung which travels a great distance (as much as two acres), and burns anything it touches.

See also the [buffalo](https://bestiary.ca/beasts/beast102099.htm).

### Illustration

The bonnacon is most often illustrated as a beast the looks like a [bull](https://bestiary.ca/beasts/beast198.htm) or [ox](https://bestiary.ca/beasts/beast199.htm), with curled horns, being attacked by men (sometimes dressed in chain mail like soldiers) with a spear or an ax. The beast is usually shown spraying its dug in defense, often with the hunters deflecting the spray with a shield.