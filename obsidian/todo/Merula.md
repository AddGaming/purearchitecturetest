#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

An agreeable bird that sings in April and May

![[img101551.jpg]]

### General Attributes

Blackbirds are all black, except those from Achaia or Arcadia. In April and May it sings marvellously high, but in the winter its song is unpleasant. Its voice is so sweet that it moves the mind to a feeling of delight. For this reason people keep blackbirds in cages.

### Allegory/Moral

The blackbird represents those who are tempted by carnal pleasures. The blackbird in flight represents the temptation to desire. To reject the desire symbolized by the blackbird, you must discipline yourself and thus rid yourself of pleasures of the mind by inflicting pain on your flesh. A white blackbird represents purity of will.

### Illustration

Blackbirds are often shown in a cage, sometimes ornate. Thet are also often depicted as a nondescript black or brown bird perched on the ground.