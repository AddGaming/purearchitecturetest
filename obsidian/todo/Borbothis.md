#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A fish that resembles an eel, but with a large head

![[img108401.jpg]]

### General Attributes

The borbothis is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It resembles an [eel](https://bestiary.ca/beasts/beast106039.htm) and can be skinned like one, and has a slippery body. Its head is large in relation to its body and it has a broad mouth. Its roe (eggs) are the best and most delectable of all fish eggs.

There is a similarly named fish, the [bothis](https://bestiary.ca/beasts/beast106180.htm), which is described entirely differently. The relation between them, if any, is unknown.