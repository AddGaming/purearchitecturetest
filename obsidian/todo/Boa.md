#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

An enormous snake, found in Italy, that feeds on cattle

![[img102120.jpg]]

### General Attributes

The boa is an enormous snake found in Italy. It feeds on cattle, but not by swallowing them; instead it sucks the milk from a cow's udder, sometimes taking so much that the cow dies.