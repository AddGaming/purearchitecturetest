#monster

## Stats

| name | value |
|------|-------|
| hp   | 1     |
| atk  | 1     |

## Drops

| name            | percentage |
|-----------------|------------|
| TODO            | x          |

## Locations

- TODO

## Lore

A fish with many large teeth

![[img107642.jpg]]

### General Attributes

The bream is found in some medieval [encyclopedias](https://bestiary.ca/prisources/psdetail105102.htm). It is a fish that is named _dentix_ for the size and great number of its teeth (_dens_).

### Reality

In modern taxonomy, _Pagrus_ ([Thomas of Cantimpré's](https://bestiary.ca/prisources/psdetail1798.htm) _peagrus_) is the genus name for a sea bream; it is in the _Sparidae_ family (sea breams). It has multiple teeth, but its teeth are not large. The species _Dentex dentex_ is also in the _Sparidae_ family; it is a predatory fish with well developed teeth that eats other fish and moluscs.