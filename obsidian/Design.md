As already mentioned in the [[Overview]], I tried to implement this project only using pure functions.
Since this project is written in Python, I had some advantages and disadvantages doing this, which I will expand on
later.

# Overview

```mermaid
flowchart LR
	main[main.py]
	
	subgraph utils [utils]
		direction LR
		errors(errors.py)
		hashing(hashing.py)
		json_2_md(json_2_md.py)
		result(result.py)
		rng(rng.py)
		string_manipulation(string_manipulation.py)
	end
	
	subgraph game [game]
		direction LR
		subgraph data [data]
			data_init(__init__)
			data_enums(enums.py)
		end
		subgraph logic [logic]
			logic_init(__init__)
		end
	end

	subgraph cli [CLI]
		direction LR
		cli_init(__init__)
		registers(action_registers.py)
		actions(actions.py)
		
		cli_init -- exposes --> factories
		cli_init -- exposes --> cli_functions
		cli_init -- exposes --> game_state
		cli_init -- exposes --> settings
		cli_init -- exposes --> cli_enums
		registers -- uses --> actions
		settings -- uses --> cli_enums
		settings -- uses --> data_enums
		settings -- uses --> result
	end
	
	cli == depends on ==> game
	cli == depends on ==> utils
	main --uses--> cli
```

# Summary

## Advantages

- Easy to test
- Easy to extend
- Clear roles of modules (group-able by type of data transformation)
- Clear criteria for coherence
- SOLID comes for free

## Disadvantages

- Cumbersome depending on language support

## Open questions

- Parallelization

# Phases of the Project

## Exploration of the Domain

## Initial Testing

## Test Driven Extension

## Optimization

### Optimizing Disc Access

Function like `game.logic.skills.all_skills()` access disc every time they get called. --> Cashing? vs Hot reload
ability

## Switching Technologies

# Learning and Discussion

## Language Choice

Python is not ideal because immutability and object construction is expensive.
Function calling is somewhat less a problem, since function objects are constructed long before executed.
But functions ARE objects, this leads to terrible performance non the less. Just python things.
Python linters are happy with this since they know that to expect

FOF are necessary.

Rust seems perfect since it doesn't clash with the borrow checker by definition.

Functional languages not good fit for games programming, but very fit for this architecture style. It's recommended best
practice there.

## Is this OOP, FP or something else?

Half functional programming since inside of functions, we can mutate and do "nonfunctional" stuff.
While OO, not OOP since messaging is a non factor.
Name wise "Procedural Programming" fits best.

## Traps

### Single Element and Multi element functions for everything

might need it later = might never need it!