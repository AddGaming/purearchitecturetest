#item

## Stats

| name | value |
|------|-------|
| atk  | 1     |

## Dropped from

| name | chance |
|------|--------|
| x    | 1      |

## Found in

- [[Sunshine Hill]]

