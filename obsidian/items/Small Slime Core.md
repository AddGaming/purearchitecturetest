#item

## Stats

| name | value |
|------|-------|
| x    | 1     |

## Dropped from

| name               | chance |
|--------------------|--------|
| [[Small Slime]]    | 1      |

## Found in

- x