#item

## Stats

| name | value |
|------|-------|
| atk  | 1     |

## Dropped from

| name       | chance |
|------------|--------|
| [[Goblin]] | 1      |

