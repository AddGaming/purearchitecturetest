#location #area 

## Connections

- [[Bear Forest]]
- [[Yofel Castle]]
- [[Lake Talpha]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Ahanes]] | 0.50000 | 20 | 30 |
| Nothing | 0.50000 | - | - |

## Lore

TODO
