#location #area 

## Connections

- [[Yarlford]]
- [[Black Iron Palace]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| Nothing | 0.50000 | - | - |
| [[Alerion]] | 0.50000 | 50 | 70 |

## Lore

TODO
