#location #area 

## Connections

- [[Cliffted Peaks]]
- [[Rust Cave]]
- [[Botama]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Furmi]] | 0.50000 | 20 | 20 |
| Nothing | 0.50000 | - | - |

## Lore

TODO
