#location #area 

## Connections

- [[Sunshine Hill]]
- [[Stawford]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Allec]] | 0.25000 | 5 | 10 |
| Nothing | 0.25000 | - | - |
| [[Astaraz]] | 0.25000 | 3 | 6 |
| [[Australis]] | 0.25000 | 40 | 45 |

## Lore

TODO
