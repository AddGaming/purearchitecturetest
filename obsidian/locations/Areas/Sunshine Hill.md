#location #area 

## Connections

- [[Panshaw]]
- [[Sunshine Forest]]
- [[Caldera Lake]]
- [[Lybster]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Small Slime]] | 0.01010 | 45 | 55 |
| [[Dirty Mucus]] | 0.18182 | - | - |
| Nothing | 0.53535 | - | - |
| [[Coreless Slime]] | 0.27273 | 1 | 3 |

## Lore

TODO
