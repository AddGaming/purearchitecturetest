#location #area 

## Connections

- [[Plane Of The Red Pillars]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Goblin]] | 0.50000 | 50 | 60 |
| Nothing | 0.50000 | - | - |

## Lore

TODO
