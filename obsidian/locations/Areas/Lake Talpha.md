#location #area 

## Connections

- [[Forest Of Wavering Mist]]
- [[Wallowdale]]
- [[Thousand Snake Swamp]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Alphoraz]] | 0.25000 | 5 | 15 |
| [[Allec]] | 0.25000 | 10 | 20 |
| Nothing | 0.25000 | - | - |
| [[Amia]] | 0.25000 | 20 | 30 |

## Lore

TODO
