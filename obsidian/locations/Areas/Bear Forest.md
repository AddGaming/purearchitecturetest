#location #area 

## Connections

- [[Sunshine Forest]]
- [[Stawford]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Ahanes]] | 0.16667 | 15 | 25 |
| [[Ripped Cloth]] | 0.16667 | - | - |
| [[Goblin]] | 0.16667 | 10 | 20 |
| Nothing | 0.16667 | - | - |
| [[Stick]] | 0.16667 | - | - |
| [[Ursa]] | 0.16667 | 20 | 25 |

## Lore

TODO
