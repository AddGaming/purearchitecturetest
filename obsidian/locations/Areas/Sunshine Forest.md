#location #area 

## Connections

- [[Sunshine Hill]]
- [[Bear Forest]]
- [[Stawford]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Goblin]] | 0.25000 | 5 | 20 |
| Nothing | 0.25000 | - | - |
| [[Ripped Cloth]] | 0.25000 | - | - |
| [[Stick]] | 0.25000 | - | - |

## Lore

TODO
