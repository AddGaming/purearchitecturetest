#location #area 

## Connections

- [[Lybster Castle]]
- [[Castle Gakey]]
- [[Silent Valley]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Skeleton]] | 0.50000 | 1 | 1 |
| Nothing | 0.50000 | - | - |

## Lore

TODO
