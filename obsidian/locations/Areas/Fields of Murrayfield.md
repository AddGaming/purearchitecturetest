#location #area 

## Connections

- [[Murrayfield]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Aeriophylon]] | 0.33333 | 80 | 90 |
| Nothing | 0.33333 | - | - |
| [[Ana]] | 0.33333 | 120 | 130 |

## Lore

TODO
