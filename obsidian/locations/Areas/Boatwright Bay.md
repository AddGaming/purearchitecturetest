#location #area 

## Connections

- [[Boatwright]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| Nothing | 0.33333 | - | - |
| [[Acato]] | 0.33333 | 1 | 1 |
| [[Abarenon]] | 0.33333 | 20 | 50 |

## Lore

TODO
