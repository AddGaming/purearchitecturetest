#location #area 

## Connections

- [[Thousand Snake Swamp]]
- [[Careford Monastery]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Amphisbaena]] | 0.33333 | 50 | 100 |
| [[Abides]] | 0.33333 | 40 | 60 |
| Nothing | 0.33333 | - | - |

## Lore

TODO
