#location #area 

## Connections

- [[Plane Of The Red Pillars]]
- [[Lybster Castle]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Anthus]] | 0.33333 | 10 | 25 |
| Nothing | 0.33333 | - | - |
| [[Asellus]] | 0.33333 | 10 | 15 |

## Lore

TODO
