#location #area 

## Connections

- [[Monument Of Swordsmen]]
- [[Forest Of Wavering Mist]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Myrmecoleon]] | 1.00000 | 40 | 50 |

## Lore

TODO
