#location #area 

## Connections

- [[Murrayfield]]
- [[Yofel Castle]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| Nothing | 0.50000 | - | - |
| [[Capeluste]] | 0.50000 | 100 | 120 |

## Lore

TODO
