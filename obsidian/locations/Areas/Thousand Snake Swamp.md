#location #area 

## Connections

- [[Lake Talpha]]
- [[Thousand Snake Castle]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Aspis]] | 0.25000 | 50 | 70 |
| [[Alphoraz]] | 0.25000 | 10 | 30 |
| [[Abides]] | 0.25000 | 30 | 50 |
| Nothing | 0.25000 | - | - |

## Lore

TODO
