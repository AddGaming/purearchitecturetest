#location #area 

## Connections

- [[Volupta]]

## Spawns

| name | probability | min level | max level |
| -- | -- | -- | -- |
| [[Aphorus]] | 0.33333 | 40 | 50 |
| [[Albirez]] | 0.33333 | 90 | 110 |
| Nothing | 0.33333 | - | - |

## Lore

TODO
