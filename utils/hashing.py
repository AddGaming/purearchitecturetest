"""
my hashes for stuff.
"""


def hash_str(string: str, max_numbers: int = 1_000_000_000) -> int:
    """
    :param string: input string
    :param max_numbers: maximum amount of digits in the output
    :return: hashes a string into an int
    """
    temp = int("".join(str(ord(c)) for c in string)) % max_numbers
    ords = sum(ord(c) for c in string)
    length = len(bin(ords))
    shifted = ords + (ords << (length - 2))
    while temp > shifted:
        shifted = ords + (shifted << (length - 2))
    return temp ^ shifted
