"""
Decorators for the project
"""
import os
import shutil
from functools import wraps


def in_own_env(fn: callable) -> callable:
    """
    A decorator that executes the test function inside its own environment.
    passes the name of the folder as a keyword argument: 'folder'
    :param fn: callable
    """

    @wraps(fn)
    def wrapper(*args, **kwargs):
        """wrapper"""
        stem = f"{os.sep.join(__file__.split(os.sep)[:-2])}{os.sep}tests{os.sep}temp"
        if not os.path.isdir(stem):
            os.mkdir(stem)
        path = f"{stem}{os.sep}{fn.__name__}"
        if not os.path.isdir(path):
            os.mkdir(path)

        fn(*args, folder=path, **kwargs)

        shutil.rmtree(path)

    return wrapper
