"""
Utility to generate up-to-date documentation based on the games data
"""
import os


def list_skills() -> (str,):
    """
    :return: list of all json paths
    """
    os.chdir(os.sep.join(__file__.split(os.sep)[:-2]))
    path_stem = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}skills{os.sep}"
    return tuple(f"{path_stem}{file_name}" for file_name in os.listdir(path_stem))


# noinspection DuplicatedCode
def convert_skills():
    """
    converts all skills to documentation
    """
    from game.data import SkillType
    from game.data import load_skills

    os.chdir(os.sep.join(__file__.split(os.sep)[:-2]))
    path_stem = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}skills{os.sep}"
    all_skills_data = load_skills(file_name.split(".")[0] for file_name in os.listdir(path_stem))
    doc_loc_stem = f"{os.getcwd()}{os.sep}obsidian{os.sep}skills{os.sep}"
    for _, skill in all_skills_data.items():
        print(f"Processing {skill.name} ...")
        old_content = ""
        if os.path.exists(f"{doc_loc_stem}{skill.name}.md"):
            with open(f"{doc_loc_stem}{skill.name}.md") as f:
                old_content = f.read()
            start = old_content.index("## Description")
            try:
                end = old_content.index("\n## ", start + 2)
            except ValueError:
                end = len(old_content)
            old_content = old_content[start:end]

        # Tags
        tags = "#skill "
        if skill.skill_type == SkillType.PHYSICAL:
            tags += "#physical"
        if skill.skill_type == SkillType.BUFFING:
            tags += "#buffing"
        if skill.skill_type == SkillType.MAGICAL:
            tags += "#magical"
        if skill.skill_type == SkillType.HIGH_MAGIC:
            tags += "#magical #high_magic"

        # Stats
        buff_str = "; ".join(f"({buff.stat}, {buff.amount}, {buff.duration})" for buff in skill.trigger_buffs)
        effect_str = "; ".join(f"({effect.name}, {effect.duration})" for effect in skill.trigger_effects)

        table = "| name | value |\n| -- | -- |\n"
        table += f"| base damage | {skill.base_damage} |\n"
        table += f"| damage scaling per lv | {skill.factor} |\n"
        table += f"| mana const | {skill.mana_cost} |\n"
        table += f"| cooldown | {skill.duration} |\n"
        table += f"| buffs | {buff_str} |\n"
        table += f"| effects | {effect_str} |\n"

        # Requires
        requirements = ""
        if not skill.requires:
            requirements += "Nothing\n"
        for skill_name in skill.requires:
            if "|" in skill_name:
                or_join = " or ".join(
                    f"[[{sn.strip().replace('_', ' ').capitalize()}]]" for sn in skill_name.split("|"))
                requirements += f"- {or_join}\n"
            else:
                requirements += f"- [[{skill_name.replace('_', ' ').capitalize()}]]\n"

        # description
        if old_content:
            description = old_content
        else:
            description = "## Description\n\nTODO\n"

        with open(f"{doc_loc_stem}{skill.name}.md", "w") as f:
            f.write(
                f"{tags}\n\n"
                f"## Stats\n\n"
                f"{table}\n"
                f"## Requires\n\n"
                f"{requirements}\n"
                f"{description}"
            )


# noinspection DuplicatedCode
def convert_monster():
    """
    converts the monster json to md in vault
    """
    from game.data import load_pawn, parse_locations
    from game.data import Pawn
    from game.logic.items import inventory_to_list

    os.chdir(os.sep.join(__file__.split(os.sep)[:-2]))
    path_stem = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}pawns{os.sep}"
    all_monster_data = (load_pawn(file_name.split(".")[0]) for file_name in os.listdir(path_stem))
    doc_loc_stem = f"{os.getcwd()}{os.sep}obsidian{os.sep}beastiary{os.sep}"

    for monster in all_monster_data:
        print(f"Processing {monster.name} ...")
        old_content = ""
        if os.path.exists(f"{doc_loc_stem}{monster.name}.md"):
            with open(f"{doc_loc_stem}{monster.name}.md") as f:
                old_content = f.read()
            start = old_content.index("## Lore")
            try:
                end = old_content.index("\n## ", start + 2)
            except ValueError:
                end = len(old_content)
            old_content = old_content[start:end]

        # Tags
        tags = "#monster "

        # stats

        stats_table = "| name | value |\n| -- | -- |\n"
        stats_table += f"| hp | {monster.stats.hit_points} |\n"
        stats_table += f"| atk | {monster.stats.attack} |\n"
        stats_table += f"| def | {monster.stats.defense} |\n"
        stats_table += f"| magic def | {monster.stats.magic_defense} |\n"
        stats_table += f"| mana | {monster.stats.mana} |\n"
        stats_table += f"| mana_regeneration | {monster.stats.mana_regeneration} |\n"
        stats_table += f"| luck | {monster.stats.luck} |\n"
        stats_table += f"| speed | {monster.stats.speed} |\n"

        # drops

        inventory_list = inventory_to_list(monster.inventory)
        chances = {
            item.name: (monster.inventory[item] / len(inventory_list))
            for item in inventory_list
        }

        drop_table = "| name | rate |\n| -- | -- |\n"
        for name, chance in chances.items():
            if name == "None":
                continue
            drop_table += f"| [[{name}]] | {chance} |\n"

        # locations

        location_stem = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}locations{os.sep}"
        all_locations_data = parse_locations(location_stem)

        spawns_in = set()
        for _, location in all_locations_data.items():
            monster_in_here = {
                spawn.name: location.level_ranges[spawn.name.lower().replace(" ", "_")]
                for spawn in location.spawns if isinstance(spawn, Pawn)
            }
            if monster.name in monster_in_here:
                spawns_in.add((location.name, monster_in_here[monster.name]))

        locations = "\n".join(f"- [[{loc}]] (lv. {level_range[0]}-{level_range[1]})" for loc, level_range in spawns_in)

        # lore
        if old_content:
            lore = old_content
        else:
            lore = "## Lore\n\nTODO\n"

        with open(f"{doc_loc_stem}{monster.name}.md", "w") as f:
            f.write(
                f"{tags}\n\n"
                f"## Stats\n\n"
                f"{stats_table}\n"
                f"## Drops\n\n"
                f"{drop_table}\n"
                f"## Locations\n\n"
                f"{locations}\n\n"
                f"{lore}"
            )


def __location_code_to_name(name: str) -> str:
    return ' '.join(e.capitalize() for e in name.split('_'))


# noinspection DuplicatedCode
def convert_locations():
    """
    converts and sorts all locations into the vault
    """
    from CLI.factories import load_locations
    from game.data import Item, Pawn

    os.chdir(os.sep.join(__file__.split(os.sep)[:-2]))
    path_stem = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}locations{os.sep}"
    all_location_data = load_locations(path_stem)
    doc_loc_stem = f"{os.getcwd()}{os.sep}obsidian{os.sep}locations{os.sep}"

    for name, location in all_location_data.items():
        print(f"Processing {location.name} ...")
        old_content = ""
        if os.path.exists(f"{doc_loc_stem}{location.name}.md"):
            with open(f"{doc_loc_stem}{location.name}.md") as f:
                old_content = f.read()
            start = old_content.index("## Lore")
            try:
                end = old_content.index("\n## ", start + 2)
            except ValueError:
                end = len(old_content)
            old_content = old_content[start:end]

        # Tags
        tags = "#location "
        if location.spawns:
            tags += "#area "
        elif not location.spawns and ("_inn" in name or "_smithy" in name or "_guild" in name):
            tags += "#building "
        else:
            tags += "#town "

        # spawns
        spawn_table = "| name | probability | min level | max level |\n| -- | -- | -- | -- |\n"
        spawn_count = len(location.spawns)
        for spawn in set(location.spawns):
            if isinstance(spawn, Item):
                spawn_table += (
                    f"| [[{spawn.name}]] | "
                    f"{sum(1 for s in location.spawns if isinstance(s, Item) and s.name == spawn.name) / spawn_count:.5f} | "
                    f"- | "
                    f"- |\n"
                )
            elif isinstance(spawn, Pawn):
                level_range = location.level_ranges["_".join(spawn.name.lower().split(" "))]
                spawn_table += (
                    f"| [[{spawn.name}]] | "
                    f"{sum(1 for s in location.spawns if isinstance(s, Pawn) and s.name == spawn.name) / spawn_count:.5f} | "
                    f"{level_range[0]} | "
                    f"{level_range[1]} |\n"
                )
            else:
                spawn_table += (
                    f"| Nothing | "
                    f"{sum(1 for s in location.spawns if s is None) / spawn_count:.5f} | "
                    f"- | "
                    f"- |\n"
                )

        # connects to
        connections = ""
        for loc_name in location.connected:
            connections += f"- [[{__location_code_to_name(loc_name)}]]\n"

        # lore
        if old_content:
            lore = old_content
        else:
            lore = "## Lore\n\nTODO\n"

        # location path
        if "#area" in tags:
            location_path = f"Areas{os.sep}{location.name}.md"
        elif "#building" in tags:
            location_path = f"Buildings{os.sep}{__location_code_to_name(name)}.md"
        else:
            location_path = f"Towns{os.sep}{location.name}.md"

        with open(f"{doc_loc_stem}{location_path}", "w") as f:
            f.write(
                f"{tags}\n\n"
                f"## Connections\n\n"
                f"{connections}\n"
                f"## Spawns\n\n"
                f"{spawn_table}\n"
                f"{lore}"
            )


if __name__ == "__main__":
    # convert_skills()
    # convert_monster()
    convert_locations()
