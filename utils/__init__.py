"""
Utility to make life easier
"""

from .rng import rng, str_to_int, rng_between
from .result import Result
from .string_manipulation import box_in, tab_right
from .hashing import hash_str
from .colors import Colors, color
