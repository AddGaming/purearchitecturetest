"""
RNG implementation and other random functions
"""


def rng(obj: any, cut_of: int = 0) -> int:
    """
    based on squirrel hash from this GDC talk https://www.youtube.com/watch?v=LWFzPP8ZbdU
    :param obj: any object that can be converted to an integer.
    If you know you feed this obj to the rng - make it integer convertible
    :param cut_of: how many digits the result should have
    :return: int - a random number
    """
    big_primes = (1236743, 2347487, 3459179)
    mangled = int(obj)
    for prime in big_primes:
        mangled *= prime  # make number big
        mangled ^= (mangled >> 8)  # mix it
        mangled >>= 8  # make number small again
    if not cut_of:
        return mangled
    else:
        return mangled % 10 ** cut_of


def rng_between(obj: any, lower: int, upper: int) -> int:
    """
    generates a random number in between the upper and lower bounds
    :param obj: any object that can be converted to an integer.
    If you know you feed this obj to the rng - make it integer convertible
    :param lower: lower bound (inclusive)
    :param upper: upper bound (inclusive)
    :return: int - a random number
    """
    num = rng(obj) % (upper - lower + 1)
    num += lower
    return num


def str_to_int(string: str) -> int:
    """
    somtimes a string will need to be passed to the rng function. This is for prior conversion
    :param string: the input
    :return: int
    """
    return sum(ord(char) for char in string)
