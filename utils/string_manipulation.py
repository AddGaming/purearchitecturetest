"""
modify strings for nicer representation in terminal
"""


def box_in(text: str, box_char: str = "#", indent: int = 1) -> str:
    """
    "draws" a box of given character around a text
    :param indent: insert tabs in front of the box
    :param text:
    :param box_char: 1 char max
    :return: boxed text
    """
    if len(box_char) > 1:
        raise ValueError(f"The parameter 'box_char' of function 'box_in' only takes strings of len 1. {box_char=}")

    split = text.split("\n")
    line_len = sorted([len(x) for x in split], reverse=True)[0]
    indent = "\t" * indent

    straight = f"{indent}{box_char * (line_len + 4)}\n"
    msg = straight
    for line in split:
        line = f"{indent}{box_char} " + line
        while len(line) < len(straight) - 3:
            line += " "
        msg += line + f" {box_char}\n"
    msg += straight

    return msg


def tab_right(string: str, amount: int = 1) -> str:
    """
    inserts tabs left of each line of the given string
    :param string: the text to tab
    :param amount: the amount of tabs
    :return: the tabbed string
    """
    return "\n".join("\t" * amount + line for line in string.split("\n"))
