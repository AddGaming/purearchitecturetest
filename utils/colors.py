"""
file with color definitions
"""
from enum import StrEnum


class Colors(StrEnum):
    """
    Color codes for terminals
    """
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def color(string: str, col: Colors) -> str:
    """
    :param string: the string to color
    :param col: the color you want
    :return: the string wrapped inside the color code
    """
    return f"{col}{string}{Colors.ENDC}"
