"""
Result type that enables to pass a value and an optional message as function return values
"""
from dataclasses import dataclass


@dataclass(frozen=True, slots=True)
class Result:
    """
    Result type that enables to pass a value and an optional message as function return values
    """
    message: str
    value: any
