"""
Collection of Errors
"""


class PawnDoesNotExist(RuntimeError):
    """
    Raise if Skill accessed does not exist
    """
    pass


class SkillDoesNotExist(RuntimeError):
    """
    Raise if Skill accessed does not exist
    """
    pass


class NotEnoughMana(RuntimeError):
    """
    Raise if Skill accessed does not exist
    """
    pass


class SkillOnCooldown(RuntimeError):
    """
    Raise if Skill accessed does not exist
    """
    pass


class SkillNotKnown(RuntimeError):
    """
    Raise if Skill accessed does not exist
    """
    pass


class RequirementsNotMet(RuntimeError):
    """
    Raise if requirements for something are not met
    """
    pass


class CorruptFile(RuntimeError):
    """
    The file that was attempted to load was malformed
    """
    pass
