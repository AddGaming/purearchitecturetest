"""
Item related functions
"""
from game.data import Item, Pawn
from utils import rng, Result
from collections.abc import Iterable

EMPTY = Item(name="None")


# print_inv -> inventory_to_str
def inventory_to_str(inventory: {Item: int}, item_names: Iterable[str] = None) -> str:
    """
    :param inventory:
    :param item_names:
    :return: user readable inventory
    """
    item_names = set(name.replace("_", " ").title() for name in item_names)
    ans = (f"{item.name}: {amount}"
           for item, amount in inventory.items()
           if amount > 0 and (not item_names or item.name in item_names))
    return "\n".join(ans)


def inventory_to_list(inventory: {Item: int}) -> [Item]:
    """
    flattens a inventory
    :param inventory: Character inventory format {Item: int}
    :return: [Item]
    """
    ans = []
    for item, count in inventory.items():
        for _ in range(count):
            ans.append(item)
    return ans


def drop_items(enemy: Pawn, player: Pawn) -> [Item]:
    """
    Given an enemy drops a random Item from that enemy inventory
    :param player:
    :param enemy:
    :return:
    """
    items = inventory_to_list(enemy.inventory)
    rgn = hash(enemy) ^ hash(player)
    loot = []
    if items:
        for _ in range(max(1, player.stats.luck // 10)):
            rgn = rng(rgn)
            loot.append(items[rgn % len(items)])

    while EMPTY in loot:
        loot.remove(EMPTY)
    return loot


def grant_item(item: Item, pawn: Pawn) -> Result:
    """
    gives an item to a pawn
    :param item: to give
    :param pawn: receiver
    :return: Result.value = new pawn
    """
    try:
        pawn.inventory[item] += 1
    except KeyError:
        pawn.inventory[item] = 1
    msg = f"\tYou found an {item.name}!\n\tYou now have {pawn.inventory[item]} {item.name}s"
    return Result(msg, pawn)


def deduct_items(pawn: Pawn, item_dict: {Item: int}) -> Pawn:
    """
    if items to be deducted are not in the inventory or the amount would go bellow 0,
    no action is executed instead.
    :param pawn: the pawn from which to deduct
    :param item_dict: the item dict in the inventory format of items and amounts to deduct
    :return: the pawn with reduced inventory
    """
    for item, amount in item_dict.items():
        if item in pawn.inventory:
            pawn.inventory[item] = min(pawn.inventory[item] - amount, 0)
    return pawn
