"""
Functions to help with talking
"""
from typing import Iterable

from game.data import DialogBranch, Player, NPC
from game.logic import quest_is_not_completed, quest_is_accepted
from utils import box_in, rng


def relevant_branches(branches: tuple[DialogBranch], player: Player) -> Iterable[DialogBranch]:
    """
    this function ignores already completed quests and already triggered dialogs branches
    :param branches:
    :param player:
    :return: list of dialog branches the player should see
    """
    return (  # branches_wo_completed_quests
        branch for branch in branches
        if not branch.quest or branch.quest.repeatable or quest_is_not_completed(branch.quest, player)
    )


def list_branches(branches: tuple[DialogBranch], player: Player) -> str:
    """
    :param branches: the raw branches
    :param player:
    :return: string of all dialog branches the player can interact with
    """
    filtered = relevant_branches(branches, player)
    options = []
    for i, db in enumerate(filtered):
        if db.quest and quest_is_accepted(db.quest, player):
            options.append(f"{i} - {db.quest.in_progress_trigger}")
        else:
            options.append(f"{i} - {db.player_input}")
    return box_in("\n".join(options), box_char="~")


def input_to_dialog_branch_mapping(branches: (DialogBranch,), player: Player) -> {str: DialogBranch}:
    """
    maps the required input from the player to the dialog branch that would trigger
    :param branches:
    :param player:
    :return: {trigger:str -> DialogBranch}
    """
    accepted_inputs = {}
    for i, db in enumerate(relevant_branches(branches, player)):
        accepted_inputs[str(i)] = db
        if db.quest and quest_is_accepted(db.quest, player):
            accepted_inputs[db.quest.in_progress_trigger.lower()] = db
        else:
            accepted_inputs[db.player_input.lower()] = db
    return accepted_inputs


def greet_player(npc: NPC, seed: int) -> (str, int):
    """
    :param npc: who greets
    :param seed: rng seed
    :return: greeting text and print speed
    """
    return npc.openers[rng(seed) % len(npc.openers)]
