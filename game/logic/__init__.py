"""
Logic for the game that works on data models defined in "data"
"""
from .stat_calculationss import max_hit_points, max_mana, str_upgradeable_stats
from .items import drop_items, grant_item, deduct_items, inventory_to_str
from .leveling import level_up, stats_up, skill_up, reward_pawn
from .fight import defend, basic_attack, apply_status, regenerate_mana, grant_experience, \
    enemy_turn, reward_player_for_enemy, tick_down, end_turn
from .skills import str_unlockable_skills, str_unlocked_skills, knowable_skills_map, use_skill, \
    active_cooldowns, all_skills
from .monster import scale_monster, level_monster
from .logic_reprs import abbreviation_to_key_word, repr_fight_status, repr_player, repr_player_detailed
from .questing import can_complete_quest, quest_is_not_completed, quest_is_accepted, accept_quest, resolve_quest, \
    handle_quest
from .dialog_functions import relevant_branches, list_branches, input_to_dialog_branch_mapping, greet_player
