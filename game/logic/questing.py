"""
Quests related functionality
"""
from game.data import Quest, Player, Item
from game.logic import deduct_items, reward_pawn
from utils import Result


def can_complete_quest(player_inventory: {Item: int}, quest_items: {Item: int}) -> bool:
    """
    :param player_inventory: the players inventory
    :param quest_items: the items required by the quest
    :return: if the given quest is completable
    """
    return all(
        item in player_inventory and amount <= player_inventory[item]
        for item, amount in quest_items.items()
    )


def quest_is_not_completed(quest: Quest, player: Player) -> bool:
    """
    :param quest: quest to check for
    :param player: player to check for
    :return: if quest is not completed yet
    """
    return hash(quest) not in player.completed_quests


def quest_is_accepted(quest: Quest, player: Player) -> bool:
    """
    :param quest: quest to check for
    :param player: player for which to check for
    :return: if quest is accepted
    """
    return hash(quest) in player.accepted_quests


def accept_quest(quest: Quest, player: Player) -> Player:
    """
    adds the quest to the list of accepted quests
    :param quest: the quest to accept
    :param player: the player to add it to
    :return: player
    """
    player.accepted_quests.add(hash(quest))
    return player


def resolve_quest(quest: Quest, player: Player) -> Result:
    """
    rewards with player with xp and items and deducts the required items from the players inventory
    :param quest: quest to be completed
    :param player: player who completed it
    :return: Result(msg, Player)
    """
    player.pawn = deduct_items(player.pawn, quest.items)
    res = reward_pawn(
        player.pawn,
        quest.xp_reward,
        items={
            Item("Bronze Coin"): quest.bronze_reward,
            Item("Silver Coin"): quest.silver_reward,
            Item("Gold Coin"): quest.gold_reward,
        })
    player.pawn = res.value
    player.accepted_quests.remove(hash(quest))
    player.completed_quests.add(hash(quest))
    return Result(res.message, player)


def handle_quest(quest: Quest, player: Player, show_dialog: callable, inquire_user: callable) -> Result:
    """
    manages the whole quest relevant logic
    :param quest: the quest in question
    :param player: the player
    :param show_dialog: function to show dialog
    :param inquire_user: function to ask user for input
    :return: Result(msg, player)
    """
    msg = ""
    if quest_is_accepted(quest, player):
        if can_complete_quest(player.pawn.inventory, quest.items):
            show_dialog(quest.completion_dialog)
            res = resolve_quest(quest, player)
            player: Player = res.value
            msg = res.message
        else:
            show_dialog(quest.in_progress_dialog)
    else:  # can always accept quest
        show_dialog(quest.proposal_dialog)
        confirmation = inquire_user(
            prompt=f"\tDo you accept this quest? (y/n): ",
            accepted={"yes": True, "y": True, "no": False, "n": False},
            fail_response="\tplease confirm or deny the quest with 'yes' or 'no'\n"
        )
        if confirmation.value:
            msg = f"\t>You have accepted the quest '{quest.quest_name}'\n"
            accept_quest(quest, player)
    return Result(msg, player)
