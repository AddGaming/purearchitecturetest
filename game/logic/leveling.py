"""
stuff with levels
"""
import io

from game.data import Pawn, StatNames, load_skills, Item
from game.logic import max_hit_points, max_mana
from utils import Result


# check_lv -> level_up
def level_up(character: Pawn) -> Result:
    """
    levels a character
    :param character: pawn
    :return: Result(msg, character-pawn)
    """
    msg = io.StringIO()
    while character.xp >= level_thresh_hold(character.lv):
        character.xp -= level_thresh_hold(character.lv)
        character.lv += 1
        character.sp += 1
        msg.write(f"\t{character.name} reached level {character.lv}\n")
    return Result(msg.getvalue(), character)


def level_thresh_hold(current_level: int) -> int:
    """
    calculates the level threshold
    :param current_level:
    :return:
    """
    return int(1.5 * current_level * 100)


def stats_up(character: Pawn, status_name: StatNames | str) -> Result:
    """
    the skill points distribution process
    :param status_name:
    :param character:
    :return:
    """
    if character.sp <= 0:
        return Result("\tYour dont have any skill points", character)

    if status_name == StatNames.HIT_POINTS:
        character.stats.hit_points_level += 1
        character.sp -= 1
        msg = f"\tYour live increased to {max_hit_points(character)}!"
    elif status_name == StatNames.ATTACK:
        character.stats.attack += 1
        character.sp -= 1
        msg = f"\tYour damage increased to {character.stats.attack}!"
    elif status_name == StatNames.DEFENSE:
        character.stats.defense += 1
        character.sp -= 1
        msg = f"\tYour defense increased to {character.stats.defense}!"
    elif status_name == StatNames.MANA:
        character.stats.mana_level += 1
        character.sp -= 1
        msg = f"\tYour max mana increased to {max_mana(character)}!"
    elif status_name == StatNames.MANA_REGENERATION:
        character.stats.mana_regeneration += 1
        character.sp -= 1
        msg = f"\tYour mana regeneration increased to {character.stats.mana_regeneration}!"
    elif status_name == StatNames.LUCK:
        character.stats.luck += 1
        character.sp -= 1
        msg = f"\tYour luck increased to {character.stats.luck}!"
    else:
        raise RuntimeError(f"{status_name=} is not considered in 'stats_up'! ")

    return Result(msg, character)


# skill_up -> unlock_skill, display_skills
def skill_up(character: Pawn, skill_name: str) -> Result:
    """
    Unlocks a skill for the character if possible
    :param character:
    :param skill_name:
    :raises SkillDoesNotExist: if the skill_name does not exist / is known as of yet
    :return:
    """
    display_name = skill_name.replace('_', ' ').capitalize()
    if skill_name in character.skills:
        character.skills[skill_name].level += 1
        character.sp -= 1
        msg = f"\tYour skill with {display_name} has reached level {character.skills[skill_name].level}!"
    else:
        character.skills |= load_skills((skill_name,))
        character.sp -= 1
        msg = f"\tYou have successfully learned the skill {display_name}!"

    return Result(msg, character)


def reward_pawn(pawn: Pawn, xp: int, items: {Item: int}) -> Result:
    """
    a general reward function that gives the player xp and items
    :param pawn:
    :param xp:
    :param items:
    :return: Result(msg, Pawn)
    """
    msg = io.StringIO()
    # xp
    pawn.xp += xp
    res = level_up(pawn)
    msg.write(res.message)
    pawn = res.value
    # items
    for item, amount in items.items():
        if not amount:
            continue
        try:
            pawn.inventory[item] += amount
        except KeyError:
            pawn.inventory[item] = amount
        msg.write(
            f"\tYou received {amount} {item.name}s\n" if amount > 1 else
            f"\tYou received {amount} {item.name}\n"
        )

    return Result(msg.getvalue(), pawn)
