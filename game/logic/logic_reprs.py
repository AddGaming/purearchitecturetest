"""
representing logical situations
"""
import io

from game.data import Pawn, Skill
from game.logic import max_hit_points, max_mana, active_cooldowns, knowable_skills_map
from game.logic.leveling import level_thresh_hold
from utils import box_in


def abbreviation_to_key_word(input_: str, player_skills: {str: Skill}) -> str:
    """
    translates the possible abbreviations to machine readable strings.
    If input doesnt match any known pattern, the input gets returned.
    :param player_skills: the players current skills
    :param input_: the player input
    :return: str - key_word
    """
    skill_names = knowable_skills_map(player_skills)
    abbreviations = {"atk": "attack", "hp": "hit_points"}

    if input_ in skill_names:
        key_word = skill_names[input_]
    elif input_ in abbreviations:
        key_word = abbreviations[input_]
    else:
        key_word = input_

    return key_word


def repr_fight_status(player: Pawn, enemy: Pawn):
    """
    Gives the relevant fight information
    :param player:
    :param enemy:
    :return:
    """
    player_feedback = __repr_player_fight_status(player)
    player_feedback += __repr_enemy_fight_status(enemy)
    return box_in(player_feedback)


def __repr_enemy_fight_status(enemy: Pawn, bc: str = "-"):
    """
    Gives the relevant fight information for the enemy
    :param bc: border_char
    :param enemy:
    :return:
    """
    ans = io.StringIO()
    if bc:
        ans.write(bc * 20 + "\n")
    ans.write(f"{enemy.name} (Lv: {enemy.lv})\n")
    # don't do tabs. It messes other formatting functions
    ans.write(f"  HP: {enemy.stats.hit_points}/{max_hit_points(enemy)}")
    # TODO: extend so that special enemies get their special bars displayed -> add parameter with param_name
    return ans.getvalue()


def __repr_player_fight_status(player: Pawn) -> str:
    """
    Gives the relevant fight information for the player
    :param player:
    :return:
    """

    ans = io.StringIO()
    ans.write(f"{player.name}\n")
    # don't do tabs. It messes other formatting functions
    ans.write(f"  HP: {player.stats.hit_points}/{max_hit_points(player)}\n")
    ans.write(f"  MANA: {player.stats.mana}/{max_mana(player)}\n")
    ans.write(f"  Cooldowns:\n")
    for skill in active_cooldowns(player.skills):
        ans.write(f"    {skill.name}: {skill.current_duration}\n")
    return ans.getvalue()


def repr_player_detailed(player: Pawn):
    """
    detailed user readable stats of the players character
    :param player:
    :return:
    """
    ans = f"{player.name} (lv. {player.lv})\n"
    sep = "-" * len(ans)
    ans += f"{sep}\n" \
           f"  hit points: {player.stats.hit_points} / {max_hit_points(player)}\n" \
           f"  mana: {player.stats.mana} / {max_mana(player)}\n" \
           f"  attack: {player.stats.attack}\n" \
           f"  defense: {player.stats.defense}\n" \
           f"  mana regeneration: {player.stats.mana_regeneration}\n" \
           f"  luck: {player.stats.luck}\n" \
           f"{sep}\n" \
           f"  buffs: {player.buffs}\n" \
           f"  status effects: {player.status_effects}\n" \
           f"{sep}\n" \
           f"  experience: {player.xp} / {level_thresh_hold(player.lv)}\n" \
           f"  skill points: {player.sp}\n"
    return box_in(ans)


def repr_player(player: Pawn):
    """
    user readable stats of the players character
    :param player:
    :return:
    """
    ans = f"{player.name} (lv. {player.lv})\n"
    if player.status_effects:
        ans += " | ".join(s.name for s in player.status_effects) + "\n"
    sep = "-" * len(ans)
    ans += f"{sep}\n" \
           f"  hit_points: {player.stats.hit_points} / {max_hit_points(player)}\n" \
           f"  mana: {player.stats.mana} / {max_mana(player)}\n" \
           f"  attack: {player.stats.attack}\n" \
           f"  defense: {player.stats.defense}\n"
    return box_in(ans)


if __name__ == "__main__":
    ...
