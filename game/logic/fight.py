"""
Fighting stuff
"""
import io

from game.data import Pawn, Buff, StatNames, StatusEffect, Difficulty, Effect, StatusEffects
from game.logic import drop_items, level_up, max_mana
from utils import Result


def defend(character: Pawn) -> Result:
    """
    Character enters defense mode
    :param character:
    :return:
    """
    buff = Buff(
        stat=StatNames.DEFENSE,
        amount=character.stats.defense // 2,
        duration=1,
        current_duration=1
    )
    character.buffs.append(buff)
    return Result("mode switched to defend", character)


def basic_attack(attacker: Pawn, defender: Pawn) -> Result:
    """
    Attacks an enemy
    :param attacker: Pawn
    :param defender: Pawn
    :return: attacker, defender
    """
    defender.stats.hit_points -= attacker.stats.attack - defender.stats.defense
    msg = f"\tbasic attack executed\n" \
          f"\tdamage dealt: {attacker.stats.attack - defender.stats.defense}"
    return Result(msg, (attacker, defender))


def apply_status(status: StatusEffect, character: Pawn) -> Pawn:
    """
    TODO: code for status effects
    applies one status condition to a character
    :param status:
    :param character:
    :return:
    """
    if status.name is StatusEffects.CONFUSED:
        ...
    elif status.name is StatusEffects.IGNITE:
        ...
    elif status.name is StatusEffects.KNOCK_UP:
        ...
    elif status.name is StatusEffects.STUN:
        ...
    else:
        # TODO: handle gracefully
        raise RuntimeError("TODO: handle gracefully: no status matched")
    return character


def regenerate_mana(character: Pawn) -> Pawn:
    """
    regenerates mana if possible
    :param character:
    :return: pawn
    """
    if character.stats.mana < max_mana(character):
        character.stats.mana += character.stats.mana_regeneration * 2
    return character


def grant_experience(character: Pawn, enemy: Pawn, difficulty: Difficulty) -> Pawn:
    """
    Gives experience to a pawn based on difficulty
    :param enemy:
    :param character:
    :param difficulty:
    :return:
    """
    character.xp += int(enemy.lv * enemy.lv / character.lv * difficulty.value)
    return character


def enemy_turn(player: Pawn, enemy: Pawn) -> Result:
    """
    Performs the enemy turn automatically
    :param player:
    :param enemy:
    :return: Result.value = (enemy, player)
    """
    msg = "\t--- enemy turn ---\n"
    for status in enemy.status_effects:
        apply_status(status, enemy)
    if enemy.stats.hit_points > 0:
        res = basic_attack(enemy, player)
    else:
        res = Result("\tThe enemy is already dead", (enemy, player))
    return Result(msg + res.message, res.value)


def reward_player_for_enemy(player: Pawn, enemy: Pawn, difficulty: Difficulty) -> Result:
    """
    rewards the player for kills
    :param player:
    :param enemy:
    :param difficulty:
    :return: Result.value = player
    """
    player = grant_experience(player, enemy, difficulty)
    result = level_up(player)
    player = result.value
    msg = io.StringIO("\n")
    msg.write(result.message)
    for drop in drop_items(enemy, player):
        try:
            player.inventory[drop] += 1
        except KeyError:
            player.inventory[drop] = 1
        msg.write(f"\tThe {enemy.name} dropped a {drop.name}!\n")
    return Result(msg.getvalue(), player)


# tick_status, tick_buffs, tick_cool_downs
def tick_down(iterable: [Effect]) -> [Effect]:
    """
    reduces the current_duration of Effects
    :param iterable:
    :return:
    """
    if isinstance(iterable, dict):
        for key, effect in iterable.items():
            if effect.current_duration > 0:
                effect.current_duration -= 1
    else:
        for effect in iterable:
            if effect.current_duration > 0:
                effect.current_duration -= 1
    return iterable


def end_turn(player: Pawn, enemy: Pawn, difficulty: Difficulty) -> Result:
    """
    All the process need when a turn is ending
    :param player:
    :param enemy:
    :param difficulty:
    :return: Result.value = player, enemy
    """
    msg = io.StringIO()
    player.skills = tick_down(player.skills)
    player.buffs = tick_down(player.buffs)
    player.status_effects = tick_down(player.status_effects)

    enemy.skills = tick_down(enemy.skills)
    enemy.buffs = tick_down(enemy.buffs)
    enemy.status_effects = tick_down(enemy.status_effects)

    player = regenerate_mana(player)
    enemy = regenerate_mana(enemy)
    if enemy.stats.hit_points <= 0:
        result = reward_player_for_enemy(player, enemy, difficulty)
        player = result.value
        msg.write(result.message)
    return Result(msg.getvalue(), (player, enemy))


if __name__ == "__main__":
    ...
