"""
Functions to evaluate character stats
"""
from game.data import Pawn, StatNames, Stats


def max_hit_points(pawn: Pawn) -> int:
    """
    potential maximum of hit points a pawn has
    :param pawn:
    :return: int - max hit points
    """
    ans = pawn.stats.base_hit_points + (pawn.stats.hit_points_level * pawn.stats.base_hit_points // 2)
    ans += sum(buff.amount for buff in pawn.buffs if buff.stat == StatNames.HIT_POINTS)
    return ans


def max_mana(pawn: Pawn) -> int:
    """
    calculates the max mana based on buffs, skill investment and base stats
    :param pawn:
    :return:
    """
    ans = pawn.stats.base_mana + pawn.stats.mana_level * 5
    ans += sum(buff.amount for buff in pawn.buffs if buff.stat == StatNames.MANA)
    return ans


def str_upgradeable_stats(stats: Stats) -> str:
    """
    :param stats:
    :return: string representing things the player can level up
    """
    return "upgradeable stats:\n  " + "\n  ".join(f"{name}: {stats.__getattribute__(name)}" for name in StatNames)
