"""
Monster stuff
"""
from game.data import Pawn, Difficulty, Location
from utils import rng_between


def level_monster(monster: Pawn, location: Location, seed: int) -> Pawn:
    """
    assigns a monster a random level based on their level range
    :param monster: the monster to scale
    :param location: the location the monster appears in
    :param seed: a seed for the rng
    :return: the monster with adjusted level
    """
    target_level = rng_between(seed, *location.level_ranges[monster.name.lower().replace(" ", "_")])
    monster.lv = target_level
    return monster


def scale_monster(monster: Pawn, difficulty: Difficulty, player_count: int = 1) -> Pawn:
    """
    live -> * difficulty
    atk -> + atk // 2 * (difficulty - 1)
    def -> + difficulty - 1
    :param monster: the
    :param difficulty: the difficulty of the game
    :param player_count: the amounts of players in the party
    :return: the updated monster
    """
    monster.stats.hit_points *= difficulty.value
    if player_count > 1:
        monster.stats.hit_points = int(monster.stats.hit_points * (player_count * 0.80))
    monster.stats.base_hit_points = monster.stats.hit_points

    monster.stats.attack += monster.stats.attack // 2 * (difficulty.value - 1)

    monster.stats.defense += difficulty.value - 1

    if player_count > 1:
        monster.stats.mana = int(monster.stats.mana * (player_count * 0.80))
    return monster

