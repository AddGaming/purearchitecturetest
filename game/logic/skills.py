"""
stuff that deals with skills
"""
import os

from game.data import Skill, Pawn, load_skills, SkillType
from game.logic import basic_attack
from utils import Result
from utils.errors import NotEnoughMana, SkillOnCooldown


def all_skills() -> {str: Skill}:
    """
    :return: all skills in format {name: Skill}
    """
    path = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}skills"
    return load_skills((name.split(".")[0] for name in os.listdir(path)), path)


def __is_skill_unlockable(skill: Skill, known_skills: dict[str: Skill]) -> bool:
    for name in skill.requires:
        if not sum(or_name.strip() in known_skills for or_name in name.split("|")):
            return False
    return True


def __unlockable_skills(known_skills: dict[str: Skill]) -> dict[str: Skill]:
    """
    returns all unlockable skills by comparing the known skills against all skills in the skills-directory
    :param known_skills:
    :return: a dict of learnable skills
    """
    skills = all_skills()
    ans = {}
    for name, skill in skills.items():
        unknown = name not in known_skills
        if unknown and __is_skill_unlockable(skill, known_skills):
            ans[name] = skill
    return ans


def str_unlockable_skills(known_skills: {str: Skill}) -> str:
    """
    Shows all unlockable skills
    :param known_skills: dict of known skills
    :return: string representation for player consumption
    """
    return "\n  ".join(skill.name for _, skill in __unlockable_skills(known_skills).items())


def str_unlocked_skills(skill_dict: {str: Skill}) -> str:
    """
    :param skill_dict:
    :return: string representation for player consumption
    """
    return "\n".join(f"{skill.name}:\tlv. {skill.level}" for _, skill in skill_dict.items())


def knowable_skills_map(player_skills: {str: Skill}) -> {str: str}:
    """
    :return: the skills the player knows the existence of mapped to the real names
    """
    unlockable = __unlockable_skills(player_skills)
    unlockable_map = {skill.name.lower(): name for name, skill in unlockable.items()}
    player_map = {skill.name.lower(): name for name, skill in player_skills.items()}

    return player_map | unlockable_map


def calc_skill_dmg(user: Pawn, target: Pawn, skill: Skill) -> int:
    """
    calculate the damage that a given spell will deal to a target
    :param user: the user of the skill
    :param target: the target of the skill
    :param skill: the skill it self
    :return: int - the damage to be inflicted
    """
    if skill.skill_type == SkillType.PHYSICAL:
        return (skill.damage + user.stats.attack) - target.stats.defense
    elif skill.skill_type == SkillType.MAGICAL:
        return skill.damage - target.stats.magic_defense
    elif skill.skill_type == SkillType.HIGH_MAGIC:
        # TODO: implement High Magic!
        raise NotImplementedError(
            f"The SkillType '{SkillType.HIGH_MAGIC}' is not yet supported"
        )
    elif skill.skill_type == SkillType.BUFFING:
        return 0  # Buffs never deal damage
    else:
        raise NotImplementedError(
            f"The SkillType '{skill.skill_type}' on skill {skill.name} is either not known or not implemented"
        )


# skill_1_use, skill_2_user, ... -> use_skill
def use_skill(user: Pawn, name: str, target: Pawn) -> Result:
    """
    :param user: skill user
    :param name: name of skill
    :param target: target of skill activation
    :raises SkillOnCooldown, NotEnoughMana:
    :return: user, target
    """
    skill: Skill = user.skills[name]
    if skill.current_duration != 0:
        if all(s.current_duration != 0 for _, s in user.skills.items()):
            result = basic_attack(user, target)
            msg = f"\tAll your skills are currently on cooldown.\n" \
                  f"\n\tbasic attack was executed instead\n" \
                  f"{result.message}"
            user, target = result.value
        else:
            msg = f"\tThe skill is still on cooldown for {skill.current_duration} more round(s)"
            raise SkillOnCooldown(msg)
    elif user.stats.mana < skill.mana_cost:
        if all(user.stats.mana < s.mana_cost for _, s in user.skills.items()):
            result = basic_attack(user, target)
            msg = f"\tYou do not have enough mana to cast any skills.\n" \
                  f"\n\tbasic attack was executed instead\n" \
                  f"{result.message}"
            user, target = result.value
        else:
            msg = f"\tYou have only {user.stats.mana} Mana but the skill requires {skill.mana_cost} Mana"
            raise NotEnoughMana(msg)

    else:
        target.stats.hit_points -= calc_skill_dmg(user, target, skill)
        # TODO: apply skill effects
        # TODO: apply skill buffs
        skill.current_duration = skill.duration
        user.stats.mana -= skill.mana_cost
        msg = f"\tskill {skill.name} was used\n" \
              f"\tdamage dealt: {skill.damage - target.stats.defense}"
    return Result(msg, (user, target))


def active_cooldowns(skills: {str: Skill}) -> [Skill]:
    """
    :param skills:
    :return: list of skills which are on cooldowns
    """
    return [skill for name, skill in skills.items() if skill.current_duration > 0]


if __name__ == "__main__":
    ...
