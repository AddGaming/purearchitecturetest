"""
Loads stuff from files
"""
import os
from functools import partial, lru_cache
from game.data import Location, Pawn, Skill, Stats, Item, Buff, StatusEffect, MetaData, Player, NPC, Dialog, \
    DialogBranch, Quest
from game.data.parser import validate_file, file_to_dataclass
from game.data.schemas import location_schema, pawn_schema, skill_schema, item_schema, meta_schema, player_schema, \
    encyclopedia_schema, NPC_schema, dialog_schema, dialog_branch_schema
from utils import tab_right


def load_skills(names: [str], path: str = "") -> {str: Skill}:
    """
    loads the skills from a given json names
    :param names: names of the skills
    :param path: location where to search for skill.json | defaults: game/data/models/skills
    :return: {skill_name: Skill}
    """
    if not path:
        path = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}skills"

    ans = {}
    for name in names:
        file_path = f"{path}{os.sep}{name}.json"
        content = validate_file(skill_schema(), file_path)
        ans[name] = file_to_dataclass(Skill, content, {})
    return ans


def __load_character_skills(skill_list: [{str: Skill}]):
    """
    loads the skills for characters which are given in the format:
    {"name": str, "level": int}
    :return:
    """
    if not skill_list or not skill_list[0]:
        return {}
    skills: {str: Skill} = load_skills(skill["name"] for skill in skill_list)
    for skill in skill_list:
        skills[skill["name"]].level = skill["level"]
    return skills


def load_items(names: (str,), path: str = "") -> dict[Item: int]:
    """
    loads item from given json names
    :param names: names of the items (duplicates allowed -> items in possession = duplicates)
    :param path: location where to search for item.json | defaults: game/data/models/items
    :return: {Item: amount}
    """
    if not path:
        path = f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}items"

    temp = {}
    for name in names:
        if name in temp:
            temp[name] = (temp[name][0], temp[name][1] + 1)
        else:
            file_path = f"{path}{os.sep}{name}.json"
            content = validate_file(item_schema(), file_path)
            temp[name] = (file_to_dataclass(Item, content, {}), 1)

    return {v[0]: v[1] for v in temp.values()}


def __spawn_to_dataclasses(names: [str]) -> list[Item | Pawn | None]:
    result = []
    model_path = f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}"
    item_names = set(s.split(".")[0].lower() for s in os.listdir(f"{model_path}items"))
    monster_names = set(s.split(".")[0].lower() for s in os.listdir(f"{model_path}pawns"))
    names = (name.lower() for name in names)

    for name in names:
        if name == "nothing":
            result.append(None)
        elif name in item_names:
            result += [item for item in load_items([name])]
        elif name in monster_names:
            monster = load_pawn(name)
            result.append(monster)
        else:
            raise RuntimeError(f"unknown name:\n{name=} could not be converted into a dataclass")

    return result


def __parse_level_range(entries: [{str: str | int}]) -> dict[str: tuple[int, int]]:
    ans = {}
    for entry in entries:
        ans[entry["monster_name"]] = (entry["min_level"], entry["max_level"])
    return ans


def __parse_npc_s(file_refs: (str,)) -> dict[str: NPC]:
    return {load_NPC(name).name.lower(): load_NPC(name) for name in file_refs}


# doesn't adhere to naming schema since load_locations already exists
@lru_cache
def parse_locations(path: str) -> dict[str: Location]:
    """
    pre parses all locations from a given directory without evaluating the registers
    :param path: locations directory
    :return: {name: location}
    """
    ans = {}
    for loc_file in os.listdir(path):
        content = validate_file(location_schema(), f"{path}{os.sep}{loc_file}")
        obj = file_to_dataclass(Location, content, {
            "spawns": __spawn_to_dataclasses,
            "level_ranges": __parse_level_range,
            "npc_s": __parse_npc_s,
        })
        ans[loc_file.split(".")[0]] = obj

    return ans


def __construct_pawn(json_content: dict) -> Pawn:
    obj: Pawn = file_to_dataclass(Pawn, json_content, {
        "stats": partial(file_to_dataclass, Stats, eval_dict={}),
        "skills": __load_character_skills,
        "inventory": load_items,
        "buffs": lambda buffs: [file_to_dataclass(Buff, buff, eval_dict={}) for buff in buffs],
        "status": lambda effects: [file_to_dataclass(StatusEffect, effect, eval_dict={}) for effect in effects],
    })
    return obj


def load_pawn(name: str, path: str = "") -> Pawn:
    """
    loads a pawn based on name from default/given location
    :param path: location where to search for pawn.json | defaults: game/data/models/pawns
    :param name: name of pawn
    :return: pawn
    """
    if not path:
        path = f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}pawns"

    file_path = f"{path}{os.sep}{name}.json"

    content = validate_file(pawn_schema(), file_path)
    return __construct_pawn(content)


def load_player(name: str, path: str = "") -> Player:
    """
    loads the player character based on name from given/default location
    :param path: location where to search for pawn.json | defaults: game/data/models/game_saves
    :param name: name of player character
    :raises IOError: If the player name has no save associated with it
    :return:
    """
    if not path:
        path = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}game_saves"

    file_path = f"{path}{os.sep}{name.lower()}.json"
    content = validate_file(player_schema(), file_path)

    return file_to_dataclass(Player, content, {
        "pawn": __construct_pawn,
        "accepted_quests": lambda xs: set(xs),
        "completed_quests": lambda xs: set(xs),
        "heard_dialogs": lambda xs: set(xs),
    })


def load_meta(path: str = "") -> MetaData:
    """
    loads meta data from default/given location
    :param path: location where to search for meta.json | defaults: game/data/models/meta
    :return: MetaData
    """
    if not path:
        path = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}meta"

    file_path = f"{path}{os.sep}meta.json"

    try:
        content = validate_file(meta_schema(), file_path)
        obj: MetaData = file_to_dataclass(MetaData, content, {})
        return obj
    except IOError:  # no such file exists
        return MetaData()


@lru_cache
def load_encyclopedia(path: str = "") -> {str: str}:
    """
    loads the encyclopedia from disc to dict for look ups
    :param path: location where to search for encyclopedia.json | defaults: game/data/models/encyclopedia
    :return: {keyword-str: explanation-str}
    """
    if not path:
        path = f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}encyclopedia"

    file_path = f"{path}{os.sep}encyclopedia.json"
    content = validate_file(encyclopedia_schema(), file_path)
    dic = {e['keyword']: tab_right(e['explanation']) for e in content}
    return dic


@lru_cache()
def load_NPC(name: str, path: str = "") -> NPC:
    """
    loads a npc by its given file name
    :param name: name of the npc
    :param path: location where to search for {name}.json | defaults: game/data/models/npcs
    :return:
    """
    if not path:
        path = f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}npcs"

    file_path = f"{path}{os.sep}{name}.json"
    content = validate_file(NPC_schema(), file_path)
    obj: NPC = file_to_dataclass(NPC, content, {
        "openers": load_dialogs,
        "root_branch": partial(load_dialog_branch),
    })
    return obj


def load_branches(names: (str,), path: str = "") -> (DialogBranch,):
    """
    :param names: loads all branches given by the names ...
    :param path: ... from the given path
    :return: tuple(DialogBranch,)
    """
    return tuple(load_dialog_branch(name, path) for name in names)


def load_dialog_branch(name: str, path: str = "") -> DialogBranch:
    """
    loads the given dialog branch into a data class
    :param name: name of the dialog_branch
    :param path: location where to search for {name}.json | defaults: game/data/models/dialog_branches
    :return:
    """
    if not path:
        path = f"{os.sep.join(__file__.split(os.sep)[:-3])}" \
               f"{os.sep}game{os.sep}data{os.sep}models{os.sep}dialog_branches"

    file_path = f"{path}{os.sep}{name}.json"
    content = validate_file(dialog_branch_schema(), file_path)
    obj: DialogBranch = file_to_dataclass(DialogBranch, content, {
        "response": partial(file_to_dataclass, Dialog, eval_dict={"follow_up": partial(load_dialog, path=path)}),
        "quest": partial(file_to_dataclass, Quest, eval_dict={
            "items": load_items,
            "in_progress_dialog": partial(file_to_dataclass, Dialog, eval_dict={"follow_up": partial(load_dialog)}),
            "completion_dialog": partial(file_to_dataclass, Dialog, eval_dict={"follow_up": partial(load_dialog)}),
            "proposal_dialog": partial(file_to_dataclass, Dialog, eval_dict={"follow_up": partial(load_dialog)}),
        }),
        "branches": partial(load_branches, path=path)
    })
    return obj


def load_dialogs(names: (str,), path: str = "") -> (Dialog,):
    """
    :param names: names of the dialog_branches
    :param path: location where to search for {name}.json | defaults: game/data/models/dialog_branches
    :return: (Dialog,)
    """
    return tuple(load_dialog(name, path) for name in names)


def load_dialog(name: str, path: str = "") -> Dialog:
    """
    loads the given dialog to a data class
    :param name: name of the dialog
    :param path: location where to search for {name}.json | defaults: game/data/models/dialogs
    :return: Dialog
    """
    if not path:
        path = f"{os.sep.join(__file__.split(os.sep)[:-3])}{os.sep}game{os.sep}data{os.sep}models{os.sep}dialogs"

    file_path = f"{path}{os.sep}{name}.json"
    content = validate_file(dialog_schema(), file_path)
    obj: Dialog = file_to_dataclass(Dialog, content, {
        "follow_up": partial(load_dialog, path=path)
    })
    return obj
