"""
Enum wrapper around integer and string based function inputs
"""
import enum


class Difficulty(enum.Enum):
    """
    How hard is the game?
    """
    EASY = 1
    NORMAL = 2
    HARD = 3


class StatNames(enum.StrEnum):
    """
    names of stats currently present
    """
    HIT_POINTS = enum.auto()
    ATTACK = enum.auto()
    DEFENSE = enum.auto()
    MANA = enum.auto()
    MANA_REGENERATION = enum.auto()
    LUCK = enum.auto()
    # SPEED = enum.auto()


class StatusEffects(enum.StrEnum):
    """
    names of status effects
    """
    STUN = enum.auto()
    KNOCK_UP = enum.auto()
    IGNITE = enum.auto()
    CONFUSED = enum.auto()


class SkillType(enum.StrEnum):
    """
    Different types of skills
    """
    PHYSICAL = enum.auto()  # normal physical attacks
    MAGICAL = enum.auto()  # normal magical attacks
    HIGH_MAGIC = enum.auto()  # require special resource <to_name>
    BUFFING = enum.auto()  # buff someone
