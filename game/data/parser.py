"""
For reading in Files (and writing them)
"""
from __future__ import annotations

import json
from dataclasses import dataclass
from jsonschema import validate, exceptions

from utils.errors import CorruptFile


def validate_file(schema: dict, path: str) -> {str: any}:
    """
    :raises IOError, CorruptFile:
    :param schema:
    :param path:
    :return: validated json dict
    """
    try:
        with open(f'{path}', "r") as f:
            content = json.loads(f.read())
    except json.decoder.JSONDecodeError:
        raise CorruptFile(f"{path} is corrupt and could not be json decoded @ {path=}")
    try:
        validate(content, schema)
    except exceptions.ValidationError:
        raise CorruptFile(f"{path} is corrupt and could not be validated for {content=}")
    return content


def file_to_dataclass(cls: dataclass, data: {str: any}, eval_dict: {str: any}) -> dataclass:
    """
    coverts json dump to dataclass
    :param cls: the class to convert into
    :param data: the json dump
    :param eval_dict: if a field needs additional evaluation, provide functions that should be applied
    :return: cls
    """
    class_dict = {}
    for key, val in data.items():
        if key in eval_dict:
            class_dict[key] = eval_dict[key](val)
        else:
            class_dict[key] = val
    return cls(**class_dict)
