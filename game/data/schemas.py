"""
collection of validation schemas
"""


def stats_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "hit_points": {
                "type": "integer",
                "minimum": 0
            },
            "base_hit_points": {
                "type": "integer",
                "minimum": 1
            },
            "hit_points_level": {
                "type": "integer",
                "minimum": 0
            },
            "attack": {
                "type": "integer"
            },
            "defense": {
                "type": "integer"
                # No minimum, since monster can be weak to physical attacks
            },
            "mana": {
                "type": "integer",
                "minimum": 0
            },
            "base_mana": {
                "type": "integer",
                "minimum": 0
            },
            "mana_level": {
                "type": "integer",
                "minimum": 0
            },
            "mana_regeneration": {
                "type": "integer"
            },
            "magic_defense": {
                "type": "integer",
                # No minimum, since monster can be weak to magic
            },
            "speed": {
                "type": "integer",
                "minimum": 0
            },
        },
        "required": [
            "hit_points",
            "base_hit_points",
            "hit_points_level",
            "attack",
            "defense",
            "mana",
            "base_mana",
            "mana_level",
            "mana_regeneration",
            "magic_defense",
            "speed"
        ]
    }


def skill_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "name": {
                "type": "string",
            },
            "requires": {
                "type": "array",
                "items": {"type": "string"}
            },
            "duration": {  # cooldown
                "type": "integer",
                "minimum": 0
            },
            "base_damage": {
                "type": "integer"
            },
            "mana_cost": {
                "type": "integer",
                "minimum": 0
            },
            "skill_type": {
                "type": "string",
                "pattern": "(physical)|(magical)|(high_magic)|(buffing)"
            },
            "current_duration": {
                "type": "integer",
                "minimum": 0
            },
            "level": {
                "type": "integer",
                "minimum": 0,
            },
            "factor": {
                "type": "number"
            },
            "trigger_buffs": {
                "type": "array",
                "items": buff_schema()
            },
            "trigger_effects": {
                "type": "array",
                "items": status_effect_schema()
            }
        },
        "required": [
            "name",
            "requires",
            "duration",
            "mana_cost",
            "base_damage",
            "level",
            "factor",
            "skill_type"
        ]
    }


def item_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "name": {
                "type": "string",
            },
        },
        "required": [
            "name",
        ]
    }


def buff_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "stat": {
                "type": "string",
            },
            "amount": {
                "type": "integer",
                "minimum": 0
            },
            "duration": {
                "type": "integer",
                "minimum": 0
            },
            "current_duration": {
                "type": "integer",
                "minimum": 0
            },
        },
        "required": [
            "stat",
            "amount",
            "duration",
            "current_duration"
        ]
    }


def status_effect_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "name": {
                "type": "string",
            },
            "duration": {
                "type": "integer",
                "minimum": 0
            },
            "current_duration": {
                "type": "integer",
                "minimum": 0
            },
        },
        "required": [
            "name",
            "duration",
            "current_duration"
        ]
    }


def pawn_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
            "stats": stats_schema(),
            "skills": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "level": {
                            "type": "integer",
                            "minimum": 0
                        }
                    }
                }
            },
            "inventory": {
                "type": "array",
                "items": {
                    "type": "string"
                },
            },
            "lv": {
                "type": "integer",
                "minimum": 1
            },
            "xp": {
                "type": "integer",
                "minimum": 0
            },
            "sp": {
                "type": "integer",
                "minimum": 0
            },
            "buffs": {
                "type": "array",
                "items": buff_schema(),
            },
            "status_effects": {
                "type": "array",
                "items": status_effect_schema(),
            },
            "level_ranges": {
                "type": "array",
                "items": {
                    "monster_name": {
                        "type": "string"
                    },
                    "min_level": {
                        "type": "integer",
                        "minimum": 0
                    },
                    "max_level": {
                        "type": "integer",
                        "minimum": 1
                    },
                }
            }
        },
        "required": [
            "name",
            "stats",
            "skills",
            "inventory",
            "lv",
        ]
    }


def location_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
            "spawns": {
                "type": "array",
                "items": {"type": "string"}
            },
            "locked_phrase": {"type": "string"},
            "register": {"type": "string"},  # name of the action register
            "connected": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "npc_s": {
                "type": "array",
                "items": {"type": "string"}
            }
        },
        "required": [
            "name",
            "locked_phrase",
            "connected",
            "register",
        ]
    }


def meta_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "action_counter": {
                "type": "integer",
                "minimum": 0
            },
        },
        "required": [
            "action_counter",
        ]
    }


def player_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "location_name": {
                "type": "string",
            },
            "pawn": pawn_schema(),
            "known_locations": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "heard_dialogs": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            },
            "accepted_quests": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            },
            "completed_quests": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            },
        },
        "required": [
            "location_name",
            "pawn",
        ]
    }


def encyclopedia_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "array",
        "items": {
            "keyword": {
                "type": "string"
            },
            "explanation": {
                "type": "string"
            },
        }
    }


def dialog_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "repeatable": {
                "type": "boolean",
            },
            "text": {
                "type": "string",
            },
            "speed": {
                "type": "number",
                "minimum": 0
            },
            "follow_up": {
                "type": "string",  # name of follow up dialog
            },
        },
        "required": [
            "repeatable",
            "text"
        ]
    }


def quest_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "repeatable": {
                "type": "boolean"
            },
            "items": {
                "type": "array",
                "items": {
                    "type": "string"
                },
            },
            "quest_name": {
                "type": "string"
            },
            "xp_reward": {
                "type": "number",
                "minimum": 0
            },
            "in_progress_dialog": dialog_schema(),
            "in_progress_trigger": {"type": "string"},
            "completion_dialog": dialog_schema(),
            "proposal_dialog": dialog_schema(),
            "bronze_reward": {
                "type": "number",
                "minimum": 0
            },
            "silver_reward": {
                "type": "number",
                "minimum": 0
            },
            "gold_reward": {
                "type": "number",
                "minimum": 0
            },
        },
        "required": [
            "repeatable",
            "items",
            "quest_name",
            "in_progress_dialog",
            "in_progress_trigger",
            "completion_dialog",
            "proposal_dialog",
        ]
    }


def dialog_branch_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "player_input": {
                "type": "string"
            },
            "response": dialog_schema(),
            "quest": quest_schema(),
            "branches": {
                "type": "array",
                "items": {
                    "type": "string"  # name of following branches
                }
            }
        },
        "required": [
            "player_input",
            "response"
        ]
    }


def NPC_schema() -> dict:
    """
    :return: Json-Validation-Schema
    """
    return {
        "type": "object",
        "properties": {
            "name": {
                "type": "string"
            },
            "openers": {
                "type": "array",
                "items": {"type": "string"},
            },
            "root_branch": {
                "type": "string",
            },
            "title": {
                "type": "string",
            },
        },
        "required": [
            "openers",
            "root_branch",
            "name"
        ]
    }
