"""
Saves Dataclasses to Files
"""
import json
import os
from dataclasses import dataclass
from enum import Enum

from game.data import MetaData, Player


def dataclass_to_file(data: dataclass, exclude: [str] = None) -> dict:
    """
    saves the settings to the save file
    :param data: dataclass - to save to file
    :param exclude: attributes to ignore when saving
    :return: None
    """
    dic = {}
    for name in data.__slots__:
        val = getattr(data, name)
        if name in exclude:
            continue
        elif isinstance(val, Enum):
            dic[name] = val.value
        elif type(val) not in (int, float, bool, str, dict, list, tuple):
            dic[name] = dataclass_to_file(val, exclude)
        else:
            dic[name] = val

    return dic


def player_to_file(player: Player, location_code: str) -> None:
    """
    Saves the player character to disc
    :param player: current player character
    :param location_code:
    :return: None
    """
    # I don't care about buffs and status effects since saving only occurs after resting
    # hence no buffs or status effects!

    # Data prep
    player_pawn = player.pawn
    raw_dump = dataclass_to_file(player_pawn, exclude=["inventory", "skills"])
    skills = {"skills": [{"name": name, "level": skill.level} for name, skill in player_pawn.skills.items()]}

    inventory = []
    for item, val in player_pawn.inventory.items():
        inventory += [item.name.lower().replace(" ", "_")] * val
    inventory = {"inventory": inventory}

    pawn_dump = raw_dump | skills | inventory

    dump = {
        "pawn": pawn_dump,
        "location_name": location_code,
        "heard_dialogs": list(player.heard_dialogs),
        "accepted_quests": list(player.accepted_quests),
        "completed_quests": list(player.completed_quests),
        # "known_locations": player.known_locations,
    }

    # save to disc
    player_path = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}game_saves{os.sep}" \
                  f"{player_pawn.name.lower()}.json"
    with open(player_path, "w") as f:
        f.write(json.dumps(dump))


def meta_to_file(meta: MetaData) -> None:
    """
    saves the meta file to default location
    :param meta: the MetaData class
    :return: None
    """
    raw_dump = dataclass_to_file(meta, ("recursion_counter",))
    meta_path = f"{os.getcwd()}{os.sep}game{os.sep}data{os.sep}models{os.sep}meta{os.sep}" \
                f"meta.json"
    with open(meta_path, "w") as f:
        f.write(json.dumps(raw_dump))


if __name__ == "__main__":
    ...
