"""
timed effects like buffs, status, cooldowns, ...
"""
from __future__ import annotations
from typing import Protocol, runtime_checkable


@runtime_checkable
class Effect(Protocol):
    """
    A timely limited status
    """
    duration: int
    current_duration: int

