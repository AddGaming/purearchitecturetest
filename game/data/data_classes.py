"""
Collection of Dataclasses for this project
Not using Pydantic since it slows down and validation already happens at json parsing (kind of)
"""
from __future__ import annotations
from dataclasses import field, dataclass

from game.data import StatusEffects, StatNames, SkillType
from utils import str_to_int, hash_str


@dataclass(slots=True)
class Pawn:
    """
    represents the character
    is hashable with divergent properties!
    """
    name: str

    stats: Stats
    skills: dict[str: Skill]
    inventory: dict[Item: int]

    # TODO: shields
    lv: int = 1  # level
    xp: int = 0  # experience
    sp: int = 0  # skill points
    buffs: list[Buff] = field(default_factory=lambda: [])
    status_effects: list[StatusEffect] = field(default_factory=lambda: [])

    def __hash__(self):
        p1, p2, p3, p4 = 1018949, 1515643, 1911929, 2312347
        h = str_to_int(self.name)
        h += sum(map(str_to_int, (str(skill) for name, skill in self.skills.items()))) % p2  # skills
        h *= p1
        h ^= self.xp
        h >>= 8
        h += sum(map(hash, self.inventory)) % p3
        h *= self.lv
        h ^= self.sp
        h += hash("".join(map(str, self.buffs))) % p4
        temp = hash("".join(map(str, self.status_effects)))
        if temp != 0:
            h *= temp
        h ^= hash(self.stats)
        return h

    @classmethod
    def dummy(
        cls, hp: int = 10, atk: int = 1, defense: int = 1, mana: int = 10, name: str = "Dummy",
        luck: int = 1, mana_level=0, hit_points_level=0
    ) -> Pawn:
        """
        creates a new dummy pawn
        """
        s = Stats(
            hit_points=hp,
            base_hit_points=10,
            hit_points_level=hit_points_level,
            attack=atk,
            defense=defense,
            mana=10,
            base_mana=mana,
            mana_level=mana_level,
            mana_regeneration=0,
            luck=luck,
            magic_defense=0,
            speed=1,
        )
        k = Skill(
            name="fire_ball", requires=[], duration=1, base_damage=2, mana_cost=3, current_duration=4, level=5,
            skill_type=SkillType.MAGICAL
        )
        i = Item(name="stick")
        p = cls(
            name=name,
            stats=s,
            skills={"fire_ball": k},
            inventory={i: 2},
        )
        return p


@dataclass(slots=True)
class Stats:
    """
    stats that an actor can hold
    """
    hit_points: int
    base_hit_points: int
    hit_points_level: int
    attack: int
    defense: int
    magic_defense: int
    mana: int
    base_mana: int
    mana_level: int
    mana_regeneration: int
    luck: int
    speed: int

    def __hash__(self):
        result = self.hit_points
        result += self.base_hit_points % 101
        result ^= self.attack
        result *= self.defense + 2
        result <<= 8
        result += self.mana
        result *= self.base_mana % 103 + 2
        result ^= self.mana_regeneration
        result *= self.luck + 2
        result >>= 8
        result += self.hit_points_level
        result *= self.mana_level % 107 + 2
        result ^= self.speed
        result *= self.magic_defense + 2

        return result


@dataclass(slots=True)
class Location:
    """
    a location the player can visit
    TODO: add unlock condition that unlocks the location if satisfied
    """
    name: str
    spawns: tuple[Pawn | Item | None]
    register: dict[str: callable] = field(  # not in json because functions are not pickleable
        default_factory=lambda: dict()
    )
    connected: tuple[str] = field(default_factory=lambda: tuple())
    locked_phrase: str = f"Location is currently unreachable"  # Text response if location is currently locked
    level_ranges: dict[str: tuple[int, int]] = field(default_factory=lambda: {})
    npc_s: dict[str: NPC] = field(default_factory=lambda: dict())


@dataclass(slots=True, frozen=True)
class Dialog:
    """
    Pieces of Dialog in the game.
    They are hashable
    """
    repeatable: bool
    text: str
    speed: int
    follow_up: Dialog = None

    def __hash__(self):
        return hash_str(self.text)


@dataclass(slots=True, frozen=True)
class Quest:
    """
    A quest that the player can accept and complete
    """
    repeatable: bool
    items: {Item: int}
    quest_name: str  # unique
    in_progress_dialog: Dialog
    in_progress_trigger: str
    completion_dialog: Dialog
    proposal_dialog: Dialog

    xp_reward: int = 0
    bronze_reward: int = 0
    silver_reward: int = 0
    gold_reward: int = 0

    def __hash__(self):
        return hash_str(self.quest_name)


@dataclass(slots=True, frozen=True)
class DialogBranch:
    """
    Represents an option in a dialog with an npc
    """
    player_input: str  # initial input to trigger
    response: Dialog
    quest: Quest = None  # if branch hit, check for quest completion / offer quest to player
    branches: [DialogBranch] = field(default_factory=lambda: tuple())


@dataclass(slots=True, frozen=True)
class NPC:
    """
    A non player character that lives in towns and can be interacted with
    """
    name: str
    openers: [Dialog]
    root_branch: DialogBranch
    title: str = ""


@dataclass(slots=True, frozen=True)
class Item:
    """
    Items
    """
    name: str


# Effects

@dataclass(slots=True)
class StatusEffect:
    """
    an ailment that afflicts the player
    """
    name: StatusEffects
    duration: int
    current_duration: int


@dataclass(slots=True)
class Buff:
    """
    Buffing a stat
    """
    stat: StatNames
    amount: int
    duration: int
    current_duration: int


@dataclass(slots=True)
class Skill:
    """
    ability that the player can use
    """
    name: str
    requires: [str]
    duration: int  # cooldown
    base_damage: int
    mana_cost: int
    skill_type: SkillType
    current_duration: int = 0
    level: int = 1
    factor: float = 1.0  # the factor that level gets multiplied by
    trigger_buffs: [Buff] = field(default_factory=lambda: tuple())
    # TODO: applicable to self | enemy
    trigger_effects: [StatusEffect] = field(default_factory=lambda: tuple())

    @property
    def damage(self) -> int:
        """
        :return: damage that the skill causes
        """
        return int(self.base_damage + self.level * self.factor)

    @classmethod
    def dummy(
        cls, name="dummy",
        requires=None,  # []
        duration=0,  # cooldown
        base_damage=1,
        mana_cost=0,
        current_duration=0,
        level=1,
        factor=1.0,  # the factor that level gets multiplied by
        skill_type=SkillType.MAGICAL
    ) -> Skill:
        """
        creates a dummy skill
        """
        requires = [] if requires is None else requires
        return cls(
            name=name,
            requires=requires,
            duration=duration,  # cooldown
            base_damage=base_damage,
            mana_cost=mana_cost,
            current_duration=current_duration,
            level=level,
            factor=factor,  # the factor that level gets multiplied by
            skill_type=skill_type
        )


@dataclass(slots=True)
class MetaData:
    """
    Meta data that the game tracks for statistics and RNG seeding.
    Completely hidden from player!
    """
    action_counter: int = 0
    recursion_counter: int = 0


@dataclass(slots=True)
class Player:
    """
    The Player character
    """
    pawn: Pawn
    location_name: str
    known_locations: [str] = field(default_factory=lambda: tuple())  # names of locations
    heard_dialogs: set[int] = field(default_factory=lambda: set())  # hashes of dialogs
    accepted_quests: set[int] = field(default_factory=lambda: set())  # hashes of quests
    completed_quests: set[int] = field(default_factory=lambda: set())  # hashes of quests
